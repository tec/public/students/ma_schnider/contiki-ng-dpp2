# Contiki-NG: The OS for Next Generation IoT Devices

[![Build Status](https://travis-ci.org/contiki-ng/contiki-ng.svg?branch=master)](https://travis-ci.org/contiki-ng/contiki-ng/branches)
[![Documentation Status](https://readthedocs.org/projects/contiki-ng/badge/?version=master)](https://contiki-ng.readthedocs.io/en/master/?badge=master)
[![license](https://img.shields.io/badge/license-3--clause%20bsd-brightgreen.svg)](https://github.com/contiki-ng/contiki-ng/blob/master/LICENSE.md)
[![Latest release](https://img.shields.io/github/release/contiki-ng/contiki-ng.svg)](https://github.com/contiki-ng/contiki-ng/releases/latest)
[![GitHub Release Date](https://img.shields.io/github/release-date/contiki-ng/contiki-ng.svg)](https://github.com/contiki-ng/contiki-ng/releases/latest)
[![Last commit](https://img.shields.io/github/last-commit/contiki-ng/contiki-ng.svg)](https://github.com/contiki-ng/contiki-ng/commit/HEAD)

Contiki-NG is an open-source, cross-platform operating system for Next-Generation IoT devices. It focuses on dependable (secure and reliable) low-power communication and standard protocols, such as IPv6/6LoWPAN, 6TiSCH, RPL, and CoAP. Contiki-NG comes with extensive documentation, tutorials, a roadmap, release cycle, and well-defined development flow for smooth integration of community contributions.

Unless explicitly stated otherwise, Contiki-NG sources are distributed under
the terms of the [3-clause BSD license](LICENSE.md). This license gives
everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.

Contiki-NG started as a fork of the Contiki OS and retains some of its original features.

Find out more:

* GitHub repository: https://github.com/contiki-ng/contiki-ng
* Documentation: https://github.com/contiki-ng/contiki-ng/wiki
* Web site: http://contiki-ng.org
* Nightly testbed runs: https://contiki-ng.github.io/testbed

Engage with the community:

* Gitter: https://gitter.im/contiki-ng
* Twitter: https://twitter.com/contiki_ng

# Contribution

This repo includes a port of Contiki-NG to the MSP432 MCU

## File structure

The following files were added to the existing Contiki-NG repo:

* arch/cpu/msp432/ includes the MCU specific files of the port
	* Makefile automatically compiles and flashed the application
	* msp432-conf.h includes adjustable configuration parameters
	* msp432-def.h includes fixed definitions
	* msp432p401r.ld is the linker script file
	* dev/gpio-hal-arch.c provides the GPIO driver
	* dev/watchdog.c provides the watchdog driver
	* p401r/clock.c provides the clock driver
	* p401r/dbg.c provides functions for serial debug print
	* p401r/int-master-arch.c provides functions to enable/disable master interrupt
	* p401r/msp432.c provides init functions as well as I2C read/write
	* p401r/rtimer-arch.c provides the driver for the rtimer
	* p401r/startup_msp432p401r_gcc.c assigns specific ISR's to interrupts
	* p401r/uart0-arch.c provides the UART driver
* arch/platform/dpp2 includes the board specific files of the port
	* contiki-conf.h includes configuration of Contiki-NG parameters
	* crc.c provides a CRC implementation
	* dpp2-def.h includes fixed board defintions (pin mappings)
	* leds-arch.c provides a driver for the LED's
	* platform.c provides the functions that are needed to initialize the board before the main execution of Contiki-NG is started
	* spi-arch.c provides an SPI driver
	* bolt/bolt.c provides the functions to interface with BOLT
	* bolt/bolt-radio.c uses the BOLT interface to implement a radio layer in the Contiki-NG network stack
	* sensors/sht30.c implements an interface to the SHT30 sensor
	* simplemac/simplemac.c implements a simple passthrough MAC for the Contiki-NG network stack
* examples/platform-specific/dpp2/ provides example applications
	* connectivity-test shows an example where nodes exchange ping and UDP messages
	* coap-server shows an example where the nodes run a CoAP server and also send registration messages to the directory server

## Usage

To run the example provided in examples/platform-specific/dpp2/coap-server/, run the following commands inside the folder:

	$ make TARGET=dpp2 NODE_ID=X coap-server.flash

where X is replaced with the node ID. The nodes IPv6 address will be derived from this ID<br/>

To start a debug session, first start the `JLinkGDBServer`:

	$ JLinkGDBServer -Device MSP432P401R -if JTAG -speed 4000 -port 2331

Then in a new terminal, inside the project folder, start `gdb-multiarch`:

	$ gdb-multiarch
    $ (gdb) target remote localhost:2331
    $ (gdb) load build/dpp2/$(PROJ_NAME).elf
    $ (gdb) file build/dpp2/$(PROJ_NAME).elf
    $ (gdb) monitor reset

After this, you can step-by-step debug your program. 

### Debugging using IDE (CLion)

With CLion, click 'Run' - 'Edit Configurations' - add a new 'GDB Remote Debug' and enter:

- GDB: `bundled GDB`
- 'target remote' args: `localhost:2331`
- Symbol file: `$(PROJ_NAME).elf`

Start `JLinkGDBServer` and `gdb-multiarch` as described above (important to reset the device, not possible from CLion). Then, start the debug session in the IDE with the above configuration.



