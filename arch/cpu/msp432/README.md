# DPP2 Programming

## Flashing

To compile and flash your project, run inside the project directory:

    $ make TARGET=dpp2 $(PROJ_NAME).flash
    

## Debugging

To setup a GDB session, start the `JLinkGDBServer`:

    $ JLinkGDBServer -Device MSP432P401R -if JTAG -speed 4000 -port 2331

Then, start a second terminal in your `$(PROJ_NAME)` folder and start `gdb-multiarch`:

    $ gdb-multiarch
    $ (gdb) target remote localhost:2331
    $ (gdb) load $(PROJ_NAME).dpp2
    $ (gdb) file $(PROJ_NAME).dpp2
    $ (gdb) monitor reset
    
After this, you can step-by-step debug your program.

### Debugging using IDE (CLion)

With CLion, click 'Run' - 'Edit Configurations' - add a new 'GDB Remote Debug' and enter:

- GDB: `bundled GDB`
- 'target remote' args: `localhost:2331`
- Symbol file: `$(PROJ_NAME).elf`

Start `JLinkGDBServer` and `gdb-multiarch` as described above (important to reset the device, not possible from CLion). Then, start the debug session in the IDE with the above configuration.