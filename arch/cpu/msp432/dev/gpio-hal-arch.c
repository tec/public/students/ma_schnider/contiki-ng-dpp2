#include "contiki.h"
#include "driverlib.h"
#include "stdint.h"
#include "dev/gpio-hal.h"
#include "rom_map.h"

//need to store cfg here
static gpio_hal_pin_cfg_t pin_configs_no_port[GPIO_HAL_CONF_PIN_COUNT] = {0};
static gpio_hal_pin_cfg_t pin_configs_port[GPIO_HAL_CONF_PORT_COUNT][GPIO_HAL_CONF_MAX_PINS_PER_PORT] = {0};

//calculate msp432 port and pin number from hal pin number(0 to GPIO_HAL_CONF_PIN_COUNT-1)
void gpio_hal_arch_to_port_pin_calc(gpio_hal_pin_t hal_pin, uint_fast8_t* port, uint_fast16_t* msp432_pin){
	//first 9 ports have 8 pins each (72 total), ports enumerating starts with 1, pins with 0
	if(hal_pin < 72){
		*port = (hal_pin / 8) + 1;
		*msp432_pin = (1 << (hal_pin % 8));
	}
	else {
		*port = 10 + ((hal_pin - 72) / 6);
		*msp432_pin = (1 << ((hal_pin - 72) % 6));
	}
}

//from the hal pin mask, calculate pin mask for a given port
void gpio_hal_arch_pinmask_to_port_pinmask_calc(gpio_hal_pin_mask_t hal_pins, uint_fast8_t port , uint_fast16_t* msp432_pin_mask){
	//first 9 ports have 8 pins
	if(port < 10) {
		*msp432_pin_mask = (hal_pins >> ((port-1) * 8)) & 0xff;
	}
	//port 10 and 11 have 6 pins
	else {
		*msp432_pin_mask = (hal_pins >> (72 + (port - 10) * 6)) & 0x3f;
	}
}

//platform specific gpio initialization
void gpio_hal_arch_init(void){
	//nothing to do here
}

//enable interrupts for a gpio pin
//only ports 1 and 2 are capable
void gpio_hal_arch_no_port_interrupt_enable(gpio_hal_pin_t pin){
	uint_fast8_t port;
	uint_fast16_t msp432_pin;
	gpio_hal_arch_to_port_pin_calc(pin, &port, &msp432_pin);
	MAP_GPIO_enableInterrupt(port, msp432_pin);

}

void gpio_hal_arch_port_interrupt_enable(gpio_hal_port_t port, gpio_hal_pin_t pin){
	MAP_GPIO_enableInterrupt(port, (1 << pin));
}

//disable interrupt for a gpio pin
void gpio_hal_arch_no_port_interrupt_disable(gpio_hal_pin_t pin){
	uint_fast8_t port;
	uint_fast16_t msp432_pin;
	gpio_hal_arch_to_port_pin_calc(pin, &port, &msp432_pin);
	MAP_GPIO_disableInterrupt(port, msp432_pin);
}

void gpio_hal_arch_port_interrupt_disable(gpio_hal_port_t port, gpio_hal_pin_t pin){
	MAP_GPIO_disableInterrupt(port, (1 << pin));
}

//Configure a gpio pin. cfg is an OR mask of GPIO_HAL_PIN_CFG_xyz
void gpio_hal_arch_no_port_pin_cfg_set(gpio_hal_pin_t pin, gpio_hal_pin_cfg_t cfg){
	uint_fast8_t port;
	uint_fast16_t msp432_pin;
	gpio_hal_arch_to_port_pin_calc(pin, &port, &msp432_pin);

	if(cfg & GPIO_HAL_PIN_CFG_PULL_UP){
		MAP_GPIO_setAsInputPinWithPullUpResistor(port, msp432_pin);
	}
	if(cfg & GPIO_HAL_PIN_CFG_PULL_DOWN){
		MAP_GPIO_setAsInputPinWithPullDownResistor(port, msp432_pin);
	}
	if(cfg & GPIO_HAL_PIN_CFG_HYSTERESIS){
		//dont know what to do, no function in lib
	}
	if(cfg & GPIO_HAL_PIN_CFG_EDGE_RISING){
		MAP_GPIO_interruptEdgeSelect(port, msp432_pin, GPIO_LOW_TO_HIGH_TRANSITION);
	}
	if(cfg & GPIO_HAL_PIN_CFG_EDGE_FALLING){
		MAP_GPIO_interruptEdgeSelect(port, msp432_pin, GPIO_HIGH_TO_LOW_TRANSITION);
	}
	if(cfg & GPIO_HAL_PIN_CFG_INT_ENABLE){
		MAP_GPIO_enableInterrupt(port, msp432_pin);
	} else {
		MAP_GPIO_disableInterrupt(port, msp432_pin);
	}
	//save the config
	pin_configs_no_port[pin] = cfg;
}

void gpio_hal_arch_port_pin_cfg_set(gpio_hal_port_t port,
                                    gpio_hal_pin_t pin,
                                    gpio_hal_pin_cfg_t cfg){
	if(cfg & GPIO_HAL_PIN_CFG_PULL_UP){
		MAP_GPIO_setAsInputPinWithPullUpResistor(port, (1 << pin));
	}
	if(cfg & GPIO_HAL_PIN_CFG_PULL_DOWN){
		MAP_GPIO_setAsInputPinWithPullDownResistor(port, (1 << pin));
	}
	if(cfg & GPIO_HAL_PIN_CFG_HYSTERESIS){
		//dont know what to do, no function in lib
	}
	if(cfg & GPIO_HAL_PIN_CFG_EDGE_RISING){
		MAP_GPIO_interruptEdgeSelect(port, (1 << pin), GPIO_LOW_TO_HIGH_TRANSITION);
	}
	if(cfg & GPIO_HAL_PIN_CFG_EDGE_FALLING){
		MAP_GPIO_interruptEdgeSelect(port, (1 << pin), GPIO_HIGH_TO_LOW_TRANSITION);
	}
	if(cfg & GPIO_HAL_PIN_CFG_INT_ENABLE){
		MAP_GPIO_enableInterrupt(port, (1 << pin));
	} else {
		MAP_GPIO_disableInterrupt(port, (1 << pin));
	}
	//save the config. port-1 because starting to count from 1, but array is 0-based
	pin_configs_port[port-1][pin] = cfg;
}

//Read the configuration of a GPIO pin
gpio_hal_pin_cfg_t gpio_hal_arch_no_port_pin_cfg_get(gpio_hal_pin_t pin){
	return pin_configs_no_port[pin];

}

gpio_hal_pin_cfg_t gpio_hal_arch_port_pin_cfg_get(gpio_hal_port_t port,
                                                  gpio_hal_pin_t pin){
	return pin_configs_port[port-1][pin];
}

//Configure a pin as GPIO input
void gpio_hal_arch_no_port_pin_set_input(gpio_hal_pin_t pin){
	uint_fast8_t port;
	uint_fast16_t msp432_pin;
	gpio_hal_arch_to_port_pin_calc(pin, &port, &msp432_pin);
	MAP_GPIO_setAsInputPin(port, msp432_pin);
}

void gpio_hal_arch_port_pin_set_input(gpio_hal_port_t port,
                                      gpio_hal_pin_t pin){
	MAP_GPIO_setAsInputPin(port, (1 << pin));
}

//Configure a pin as GPIO output
void gpio_hal_arch_no_port_pin_set_output(gpio_hal_pin_t pin){
	uint_fast8_t port;
	uint_fast16_t msp432_pin;
	gpio_hal_arch_to_port_pin_calc(pin, &port, &msp432_pin);
	MAP_GPIO_setAsOutputPin(port, msp432_pin);
}

void gpio_hal_arch_port_pin_set_output(gpio_hal_port_t port,
                                       gpio_hal_pin_t pin){
	MAP_GPIO_setAsOutputPin(port, (1 << pin));
}

//Set a GPIO pin to logical high
void gpio_hal_arch_no_port_set_pin(gpio_hal_pin_t pin){
	uint_fast8_t port;
	uint_fast16_t msp432_pin;
	gpio_hal_arch_to_port_pin_calc(pin, &port, &msp432_pin);
	MAP_GPIO_setOutputHighOnPin(port, msp432_pin);
}

void gpio_hal_arch_port_set_pin(gpio_hal_port_t port, gpio_hal_pin_t pin){
	MAP_GPIO_setOutputHighOnPin(port, (1 << pin));
}

//Clear a GPIO pin (logical low)
void gpio_hal_arch_no_port_clear_pin(gpio_hal_pin_t pin){
	uint_fast8_t port;
	uint_fast16_t msp432_pin;
	gpio_hal_arch_to_port_pin_calc(pin, &port, &msp432_pin);
	MAP_GPIO_setOutputLowOnPin(port, msp432_pin);
}

void gpio_hal_arch_port_clear_pin(gpio_hal_port_t port, gpio_hal_pin_t pin){
	MAP_GPIO_setOutputLowOnPin(port, (1 << pin));
}

//Toggle a GPIO pin
void gpio_hal_arch_no_port_toggle_pin(gpio_hal_pin_t pin){
	uint_fast8_t port;
	uint_fast16_t msp432_pin;
	gpio_hal_arch_to_port_pin_calc(pin, &port, &msp432_pin);
	MAP_GPIO_toggleOutputOnPin(port, msp432_pin);
}

void gpio_hal_arch_port_toggle_pin(gpio_hal_port_t port, gpio_hal_pin_t pin){
	MAP_GPIO_toggleOutputOnPin(port, (1 << pin));
}

//Read a GPIO pin
uint8_t gpio_hal_arch_no_port_read_pin(gpio_hal_pin_t pin){
	uint_fast8_t port;
	uint_fast16_t msp432_pin;
	gpio_hal_arch_to_port_pin_calc(pin, &port, &msp432_pin);
	return MAP_GPIO_getInputPinValue(port, msp432_pin);
}

uint8_t gpio_hal_arch_port_read_pin(gpio_hal_port_t port, gpio_hal_pin_t pin){
	return MAP_GPIO_getInputPinValue(port, (1 << pin));
}

//Write a GPIO pin
void gpio_hal_arch_no_port_write_pin(gpio_hal_pin_t pin, uint8_t value){
	if(value == 0){
		gpio_hal_arch_no_port_clear_pin(pin);
	}
	else {
		gpio_hal_arch_no_port_set_pin(pin);
	}
}

void gpio_hal_arch_port_write_pin(gpio_hal_port_t port,
                                  gpio_hal_pin_t pin,
                                  uint8_t value){
	if(value == 0){
		gpio_hal_arch_port_clear_pin(port,pin);
	}
	else {
		gpio_hal_arch_port_set_pin(port,pin);
	}
}

//Set multiple pins to logical high
//right now pin mask supports only 64 bit, therefore only pins 0-63 supported
void gpio_hal_arch_no_port_set_pins(gpio_hal_pin_mask_t pins){
	uint_fast16_t msp432_pin_mask = 0;
	for(uint_fast8_t port = 1; port < 12; port++) {
		gpio_hal_arch_pinmask_to_port_pinmask_calc(pins, port, &msp432_pin_mask);
		MAP_GPIO_setOutputHighOnPin(port, msp432_pin_mask);
	}

}

void gpio_hal_arch_port_set_pins(gpio_hal_port_t port,
                                 gpio_hal_pin_mask_t pins){
	MAP_GPIO_setOutputHighOnPin(port, pins);
}

//Clear multiple pins to logical low
void gpio_hal_arch_no_port_clear_pins(gpio_hal_pin_mask_t pins){
	uint_fast16_t msp432_pin_mask = 0;
	for(uint_fast8_t port = 1; port < 12; port++) {
		gpio_hal_arch_pinmask_to_port_pinmask_calc(pins, port, &msp432_pin_mask);
		MAP_GPIO_setOutputLowOnPin(port, msp432_pin_mask);
	}
}

void gpio_hal_arch_port_clear_pins(gpio_hal_port_t port,
                                   gpio_hal_pin_mask_t pins){
	MAP_GPIO_setOutputLowOnPin(port, pins);
}

void gpio_hal_arch_port_toggle_pins(gpio_hal_port_t port,
                                    gpio_hal_pin_mask_t pins){
	MAP_GPIO_toggleOutputOnPin(port, pins);
}

//Read multiple pins
gpio_hal_pin_mask_t gpio_hal_arch_no_port_read_pins(gpio_hal_pin_mask_t pins){
	//can only do ports 1-8, because mask only 64 bit
	gpio_hal_pin_mask_t values_return = 0;
	uint_fast16_t msp432_pin_mask = 0;
	uint8_t pin_value = 0;


	for(uint_fast8_t port = 1; port < 9; port++){
		gpio_hal_arch_pinmask_to_port_pinmask_calc(pins, port, &msp432_pin_mask);
		//can only read single pins with lib functions. each port has 8 pins
		for(uint_fast16_t pin_iterator = 0; pin_iterator < GPIO_HAL_CONF_MAX_PINS_PER_PORT; pin_iterator++){
			if(((1 << pin_iterator) & msp432_pin_mask) == 0 ){
				//dont read this pin, set to 0 in return
				continue;

			}
			else {
				pin_value = MAP_GPIO_getInputPinValue(port, (1 << pin_iterator));
			}
			values_return = values_return | (pin_value << ((port-1) * 8 + pin_iterator));
		}
			
	}
	return values_return;
}

gpio_hal_pin_mask_t gpio_hal_arch_port_read_pins(gpio_hal_port_t port,
                                                 gpio_hal_pin_mask_t pins){
	gpio_hal_pin_mask_t values_return = 0;
	uint8_t pin_iterator = 0;
	uint8_t pin_value = 0;

	for(pin_iterator = 0; pin_iterator < GPIO_HAL_CONF_MAX_PINS_PER_PORT; pin_iterator++){
		if((pins & (1 << pin_iterator)) == 0){
			//dont read this pin
			continue;
		}
		else{
			pin_value = MAP_GPIO_getInputPinValue(port, (1 << pin_iterator));
			values_return = values_return | (pin_value << pin_iterator);
		}
	}
	return values_return;
}

//Write multiple pins
void gpio_hal_arch_no_port_write_pins(gpio_hal_pin_mask_t pins, gpio_hal_pin_mask_t value){
	uint8_t pin_iterator = 0;
	for(pin_iterator = 0; pin_iterator < sizeof(gpio_hal_pin_mask_t); pin_iterator++){
		//check if this pin is set in the pin mask
		if((pins & (1 << pin_iterator)) != 0) {
			if((value & (1 << pin_iterator)) == 0){
				gpio_hal_arch_no_port_write_pin(pin_iterator, 0);
			}
			else{
				gpio_hal_arch_no_port_write_pin(pin_iterator, 1);
			}
		}
	}
}

void gpio_hal_arch_port_write_pins(gpio_hal_port_t port,
                                   gpio_hal_pin_mask_t pins,
                                   gpio_hal_pin_mask_t value){
	uint8_t pin_iterator = 0;
	for(pin_iterator = 0; pin_iterator < GPIO_HAL_CONF_MAX_PINS_PER_PORT; pin_iterator++){
		//check if this pin is set in the pin mask
		if((pins & (1 << pin_iterator)) != 0){
			//read value from the mask
			if((value & (1 << pin_iterator)) == 0){
				gpio_hal_arch_port_write_pin(port, pin_iterator, 0);
			}
			else{
				gpio_hal_arch_port_write_pin(port, pin_iterator, 1);
			}
		}
	}
}
