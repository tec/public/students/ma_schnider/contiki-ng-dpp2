#ifndef UART0_ARCH_H_
#define UART0_ARCH_H_

#include "stdint.h"
#include "stddef.h"

void uart0_init(void);
void uart0_write_byte(uint_fast8_t byte);
void uart0_write(const char *buf, size_t buf_size);
void EUSCIA0_IRQHandler(void);

#endif /* UART0_ARCH_H_ */