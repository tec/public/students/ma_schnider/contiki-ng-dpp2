#include "contiki.h"
#include "dev/watchdog.h"
#include "driverlib.h"
#include "rom_map.h"

//for nested calls
static int counter = 0;

void watchdog_init(void){
	counter = 0;
	watchdog_stop();
	MAP_SysCtl_setWDTTimeoutResetType(SYSCTL_SOFT_RESET);
	MAP_WDT_A_initWatchdogTimer(WDT_A_CLOCKSOURCE_SMCLK, WDT_A_CLOCKITERATIONS_2G);
}

void watchdog_start(void){
	counter--;
	if(counter == 0){
		MAP_WDT_A_startTimer();
	}
}

void watchdog_periodic(void){
	MAP_WDT_A_clearTimer();
}

void watchdog_stop(void){
	counter++;
	if(counter == 1){
		MAP_WDT_A_holdTimer();
	}
}

void watchdog_reboot(void){
	MAP_ResetCtl_initiateHardReset();
}
