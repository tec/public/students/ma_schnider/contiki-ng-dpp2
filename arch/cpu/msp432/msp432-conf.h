#ifndef MSP432_CONF_H_
#define MSP432_CONF_H_

#include "driverlib.h"

/* Clocks */
#define HFXTCLK_SPEED           48000000                // external high-frequency crystal oscillator clock speed [Hz]
#define LFXTCLK_SPEED           32768                   // external low-frequency crystal oscillator clock speed [Hz]
#define DCOCLK_SPEED            3000000                 // internal digitally-controlled oscillator
#define REFOCLK_SPEED           32768                   // 32 or 128 kHz
#define MCLK_SRC                CS_HFXTCLK_SELECT       // main/master clock source
#define MCLK_DIV                CS_CLOCK_DIVIDER_1      // main clock divider
#define MCLK_SPEED              HFXTCLK_SPEED           // main clock speed [Hz]
#define SMCLK_SRC               CS_HFXTCLK_SELECT       // sub-system master clock
#define SMCLK_DIV               CS_CLOCK_DIVIDER_16      //
#define SMCLK_SPEED             (HFXTCLK_SPEED / 16)     // 6 MHz
#define HSMCLK_SRC              CS_HFXTCLK_SELECT       // high-speed sub-system master clock
#define HSMCLK_DIV              CS_CLOCK_DIVIDER_4      //
#define HSMCLK_SPEED            (HFXTCLK_SPEED / 4)     // 12 MHz
#define ACLK_SRC                CS_LFXTCLK_SELECT       // auxiliary clock
#define ACLK_DIV                CS_CLOCK_DIVIDER_1      //
#define ACLK_SPEED              LFXTCLK_SPEED           // max, PIN 128 kHz
#define BCLK_SRC                CS_LFXTCLK_SELECT       // low-speed backup domain clock, LFXT or REFO
#define BCLK_DIV                CS_CLOCK_DIVIDER_1      //
#define BCLK_SPEED              LFXTCLK_SPEED           // max, PIN 32 kHz

#define CLOCK_CONF_SECOND       256UL

/* SVS (supply voltage supervisor) */
#define SVS_ENABLE              0                       // enable SVSHM and SVSL
#define SVS_LOW_PERF            1                       // low performance mode

/* Debug UART */
#define UART_BAUDRATE           115200
#define UART_CLK_SRC            EUSCI_A_CTLW0_SSEL__SMCLK
#define UART_CLK_SPEED          SMCLK_SPEED
#define UART_MODULE             EUSCI_A0

/* SPI */
#define SPI_CLK_SRC             UCSSEL__SMCLK           // must be UCSSEL__ACLK or UCSSEL__SMCLK
#define SPI_CLK_SPEED           SMCLK_SPEED             // adjust this according to the SPI_CLK_SRC selection
#define SPI_FAST_READ           0
#define SPI_DUMMY_BYTE          0xff

/* I2C */
#define I2C_SCL_SPEED           100000
#define I2C_CLK_SRC             EUSCI_B_CTLW0_SSEL__SMCLK
#define I2C_CLK_SPEED           SMCLK_SPEED
#define I2C_MODULE              EUSCI_B3                // default module for I2C sensors
#define I2C_MODULE_ADDR         EUSCI_B3_BASE           // default module for I2C sensors

/* SD Card */
#define SDCARD_SPI_MODULE       EUSCI_A1_SPI
#define SDCARD_SPI_CPOL         0                       // active low
#define SDCARD_SPI_CPHA         0                       // data changed on first edge, captured on next (note: use UCBx module if phase = 1 is required! see erratasheet)
#define SDCARD_SPI_SPEED        400000                  // for compatibility reasons, initial clock should be 400kHz
#define SDCARD_SPI_SPEED_FAST   SMCLK_SPEED
#define SDCARD_BLOCK_SIZE       512

/* ADC */
#define ADC_NUM_CH              24                      //  24 channels for the 100-pin version, 12 channels for the 64-pin version


/* BOLT */
#define BOLT_USE_DMA            0
#define BOLT_SPI_SPEED          (SMCLK_SPEED / 2)
#define BOLT_SPI_MODULE         EUSCI_B0_SPI
#define BOLT_SPI_CPOL           0                       // default clock polarity (0 = inactive low)
#define BOLT_SPI_CPHA           0                       // default clock phase (1 = data changed on first edge, captured on next)

/* Systick timer */
#define SYSTICK_PERIOD          (MCLK_SPEED / 100)      // max, PIN value is 24-bit (~16M), this yiels a min. divider of 3 and a period of ~350ms (with a 48MHz MCLK); the divider should be an even number

/* Timer */
#define TIMER_A0_SRC            TASSEL__SMCLK
#define TIMER_A1_SRC            TASSEL__ACLK

/* PWM timer */
#define PWM_PERIOD              0xffff

/* 32-bit timer */
#define TIMER32_PRESCALER       TIMER32_PRESCALER_256    // select 1, 16 or 256
#define TIMER32_SPEED           (MCLK_SPEED / ((TIMER32_PRESCALER == TIMER32_PRESCALER_256) ? 256 : (TIMER32_PRESCALER == TIMER32_PRESCALER_16 ? 16 : 1)))  // number of ticks per second

/* rtimer size */
#define RTIMER_CONF_CLOCK_SIZE 4


#endif /* MSP432_CONF_H_ */