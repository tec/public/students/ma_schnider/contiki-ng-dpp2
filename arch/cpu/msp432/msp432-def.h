#ifndef MSP430_DEF_H_
#define MSP430_DEF_H_
#include <stdint.h>
#include "driverlib.h"

/* These names are deprecated, use C99 names. */
typedef  uint8_t    u8_t;
typedef uint16_t   u16_t;
typedef uint32_t   u32_t;
typedef  int32_t   s32_t;

/* Types for clocks and uip_stats */
typedef unsigned short uip_stats_t;
typedef unsigned long clock_time_t;
typedef long off_t;

/* Flash memory */
#define FLASH_MEMORY_START      0x00000000              // first valid word in the flash memory
#define FLASH_MEMORY_SIZE       (262144 / 2)            // total size of the flash memory


/*
 * D E F I N I T I O N S
 */


#define PIN0    0
#define PIN1    1
#define PIN2    2
#define PIN3    3
#define PIN4    4
#define PIN5    5
#define PIN6    6
#define PIN7    7

#define PORT1   1
#define PORT2   2
#define PORT3   3
#define PORT4   4
#define PORT5   5
#define PORT6   6
#define PORT7   7
#define PORT8   8
#define PORT9   9
#define PORT10  10
#define PORTJ   11

//function headers from msp432.c
void msp432_init(void);
void spi_b_init(EUSCI_B_SPI_Type* spi_module, uint32_t sck_speed, uint_fast8_t cpol, uint_fast8_t cpha);
void i2c_init(EUSCI_B_Type* i2c_module, uint32_t scl_speed);
uint_fast8_t i2c_read(EUSCI_B_Type* i2c_module, uint8_t slave_addr, uint16_t cmd, uint32_t num_bytes, uint8_t* out_data);
uint_fast8_t i2c_read_cmd8(EUSCI_B_Type* i2c_module, uint8_t slave_addr, uint8_t cmd, uint32_t num_bytes, uint8_t* out_data);
uint_fast8_t i2c_write(EUSCI_B_Type* i2c_module, uint8_t slave_addr, uint16_t cmd);
uint_fast8_t i2c_write_cmd8(EUSCI_B_Type* i2c_module, uint8_t slave_addr, uint8_t cmd);


/*
 * M A C R O S
 */

#define MAX(x, y)                       ( (x) > (y) ? (x) : (y) )
#define MIN(x, y)                       ( (x) < (y) ? (x) : (y) )
#define PIN_TO_BIT(pin)                 (1 << pin)
#define REGVAL32(x)                     (*((volatile uint32_t *)((uint32_t)x)))
#define REGVAL16(x)                     (*((volatile uint16_t *)((uint32_t)x)))
#define REGVAL8(x)                      (*((volatile uint8_t *)((uint32_t)x)))

/* note: there is a factor of 2 difference btw. the parameter and the actual
 * no. of delayed cycles */
/*
#define WAIT(s)                         SysCtlDelay(MCLK_SPEED / 6 * s)
#define WAIT_MS(ms)                     SysCtlDelay(MCLK_SPEED / 6000 * ms)
#define WAIT_US(us)                     SysCtlDelay(MCLK_SPEED / 6000000 * us)*/  //__delay_cycles(MCLK_SPEED / 3000000 * us)
#define WAIT(s)                         clock_delay_usec(s * 1000000)
#define WAIT_MS(ms)                     clock_delay_usec(ms * 1000)
#define WAIT_US(us)                     clock_delay_usec(us)

/* Note: all following macros ending with '_I' (immediate) can only be used
   when passing numbers directly (no defines or variables) */
#define PORT_XOR_I(port)                P##port##OUT ^= 0xff
#define PORT_SET_I(port)                P##port##OUT = 0xff
#define PORT_CLR_I(port)                P##port##OUT = 0x00
#define PORT_SEL_PRI_I(port)            { P##port##SEL0 = 0xff; P##port##SEL1 = 0x00; }
#define PORT_UNSEL_I(port)              { P##port##SEL0 = 0x00; P##port##SEL1 = 0x00; }
#define PORT_RES_EN_I(port)             P##port##REN = 0xff
#define PORT_CLR_IFG_I(port)            P##port##IFG = 0x00
#define PORT_CFG_OUT_I(port)            P##port##DIR = 0xff
#define PORT_CFG_IN_I(port)             P##port##DIR = 0x00
#define PIN_XOR_I(port, pin)            P##port##OUT ^= BIT##pin
#define PIN_SET_I(port, pin)            P##port##OUT |= BIT##pin
#define PIN_CLR_I(port, pin)            P##port##OUT &= ~BIT##pin
#define PIN_SEL_PRI_I(port, pin)        { P##port##SEL0 |= BIT##pin; P##port##SEL1 &= ~BIT##pin; }
#define PIN_UNSEL_I(port, pin)          { P##port##SEL0 &= ~BIT##pin; P##port##SEL1 &= ~BIT##pin; }
#define PIN_CFG_OUT_I(port, pin)        P##port##DIR |= BIT##pin
#define PIN_CFG_IN_I(port, pin)         P##port##DIR &= ~BIT##pin
#define PIN_CLR_IFG_I(port, pin)        P##port##IFG &= ~BIT##pin
#define PIN_RES_EN_I(port, pin)         P##port##REN |= BIT##pin
#define PIN_IES_RISING_I(port, pin)     P##port##IES &= ~BIT##pin
#define PIN_IES_FALLING_I(port, pin)    P##port##IES |= BIT##pin
#define PIN_IES_TOGGLE_I(port, pin)     P##port##IES ^= BIT##pin
#define PIN_INT_EN_I(port, pin)         P##port##IE |= BIT##pin
#define PIN_IFG_I(port, pin)            (P##port##IFG & (uint8_t)(BIT##pin))
#define PIN_GET_I(port, pin)            (P##port##IN & (uint8_t)(BIT##pin))
#define PIN_CFG_INT_I(port, pin, r)     { PIN_CFG_IN_I(port, pin); \
                                          if (r) { PIN_IES_RISING_I(port, pin); PIN_PULLDOWN_EN_I(port, pin); } \
                                          else { PIN_IES_FALLING_I(port, pin); PIN_PULLUP_EN_I(port, pin); } \
                                          PIN_CLR_IFG_I(port, pin); \
                                          PIN_INT_EN_I(port, pin); }
#define PIN_PULLUP_EN_I(port, pin)      { PIN_RES_EN_I(port, pin); PIN_SET_I(port, pin); }
#define PIN_PULLDOWN_EN_I(port, pin)    { PIN_RES_EN_I(port, pin); PIN_CLR_I(port, pin); }
#define PIN_MAP_I(port, pin, map)       { PMAPKEYID = PMAP_KEYID_VAL; \
                                          PMAPCTL |= PMAPRECFG; \
                                          *((volatile uint8_t*)&P##port##MAP01 + pin) = map; \
                                          PMAPKEYID = 0; }

#define PIN_XOR(p)                      PIN_XOR_I(p)                // toggle output bit/level
#define PIN_SET(p)                      PIN_SET_I(p)                // set output high
#define PIN_CLR(p)                      PIN_CLR_I(p)                // clear (set output low)
#define PIN_SEL_PRI(p)                  PIN_SEL_PRI_I(p)            // primary module function
#define PIN_UNSEL(p)                    PIN_UNSEL_I(p)              // unselect (configure port function)
#define PIN_CFG_OUT(p)                  PIN_CFG_OUT_I(p)            // configure pin as output
#define PIN_CFG_IN(p)                   PIN_CFG_IN_I(p)             // configure pin as input
#define PIN_CLR_IFG(p)                  PIN_CLR_IFG_I(p)            // clear interrupt flag
#define PIN_IES_RISING(p)               { PIN_IES_RISING_I(p); PIN_CLR_IFG_I(p); }    // interrupt edge select (always clear the IFG when changing PxIES)
#define PIN_IES_FALLING(p)              { PIN_IES_FALLING_I(p); PIN_CLR_IFG_I(p); }   // interrupt edge select
#define PIN_IES_TOGGLE(p)               { PIN_IES_TOGGLE_I(p); PIN_CLR_IFG_I(p); }    // interrupt edge toggle
#define PIN_INT_EN(p)                   PIN_INT_EN_I(p)             // enable port interrupt
#define PIN_CFG_INT(p, rising)          PIN_CFG_INT_I(p, rising)    // configure & enable port interrupt
#define PIN_IFG(p)                      PIN_IFG_I(p)                // get interrupt flag
#define PIN_GET(p)                      PIN_GET_I(p)                // get input bit/level
#define PIN_PULLUP_EN(p)                PIN_PULLUP_EN_I(p)          // enable pullup resistor (for input pins only)
#define PIN_PULLDOWN_EN(p)              PIN_PULLDOWN_EN_I(p)        // enable pulldown resistor (for input pins only)
#define PIN_MAP(p, map)                 PIN_MAP_I(p, map)

#define GPIO_HAL_CONF_ARCH_SW_TOGGLE	0
#define GPIO_HAL_CONF_PIN_COUNT			84
//use port and pin numbers, instead just a pin number
#define GPIO_HAL_PORT_PIN_NUMBERING	   1
#define GPIO_HAL_CONF_PORT_COUNT       11
#define GPIO_HAL_CONF_MAX_PINS_PER_PORT   8
/*
	enumerated in the sequence: p1.0-p1.7, p2.0-p2.7, p3.0-p3.7, p4.0-p4.7, p5.0-5.7, p6.0-p6.7, p7.0-p7.7, p8.0-p8.7,
	p9.0-p9.7, p10.0-p10.5, pJ.0-pJ.5, 9 Ports with 8 pins, 2 ports with 6 pins = 84
*/

/* interrupts */
#define INTERRUPT_MASTER_ENABLED        (CPU_primask() == 0)
#define INTERRUPT_ENABLE(i)             NVIC->ISER[(i - 16) / 32] = (1 << (((i) - 16) & 31))     // i <= 56 (e.g. INT_PORT3)

/* watchdog */
#define WATCHDOG_STOP                   WDTCTL = (WDTPW | WDTHOLD)
#define WATCHDOG_START                  WDTCTL = ((WDTCTL & ~(WDTHOLD)) + WDTPW)
#define WATCHDOG_RESET                  WDTCTL = ((WDTCTL | WDTCNTCL) + WDTPW)


/* SPI */
#define SPI_ENABLE(module)              ((module)->CTLW0 &= ~UCSWRST)         //( UC##module##CTLW0 &= ~UCSWRST )
#define SPI_IS_ENABLED(module)          (((module)->CTLW0 & UCSWRST) == 0)    //( !(UC##module##CTLW0 & UCSWRST) )
#define SPI_DISABLE(module)             ((module)->CTLW0 |= UCSWRST)          //( UC##module##CTLW0 |= UCSWRST )
#define SPI_WAIT_TXE(module)            while (!((module)->IFG & UCTXIFG))    //while (!(UC##module##IFG & UCTXIFG))
#define SPI_WAIT_RXNE(module)           while (!((module)->IFG & UCRXIFG))    //while (!(UC##module##IFG & UCRXIFG))
/* important: in Rev.B devices, BUSY bit is NOT usable for USCIA module! */
#define SPI_WAIT_BUSY(module)           while ((module)->STATW & UCBUSY)  //while (UC##module##STATW & UCBUSY)
#define SPI_IS_BUSY(module)             ((module)->STATW & UCBUSY)        //( UC##module##STATW & UCBUSY )
#define SPI_WRITE_BYTE(module, b)       SPI_WAIT_TXE(module); (module)->TXBUF = (b)
#define SPI_READ_BYTE(module, b)        SPI_WRITE_BYTE(module, SPI_DUMMY_BYTE); SPI_WAIT_RXNE(module); (b) = (module)->RXBUF   //{ SPI_WAIT_RXNE(module); (b) = UC##module##RXBUF; }
#define SPI_CLR_RXIFG(module)           (module)->IFG &= (~UCRXIFG)       // or do a read from the RX buffer: (void)((module)->RXBUF)   or   ((void)UC##module##RXBUF)

#endif /* MSP430_DEF_H_ */
