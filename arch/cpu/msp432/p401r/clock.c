#include "contiki.h"
#include "sys/clock.h"
#include "msp432-def.h"
//#include "rom_map.h"
#include "driverlib.h"
#include "clock-arch.h"
#include "sys/etimer.h"
#include "stdio.h"
#include "rom_map.h"

static volatile unsigned long overflow_counter;
static volatile unsigned long seconds;
#define INTERVAL (MAP_CS_getMCLK() / CLOCK_CONF_SECOND)

void clock_init(void){

  /*
  * configure all clocks:
  * - MCLK  (master clock)
  * - ACLK  (auxillary clock, max. 128 kHz)
  * - HSMCLK  (high-speed sub-system master clock)
  * - SMCLK   (low-speed sub-system master clock, same source as HSMCLK)
  * - BCLK  (low-speed backup domain clock, LFXT or REFO, max. 32 kHz)
  *
  * available clock sources are:
  * - LFXTCLK (external low-freq. osc., max. 32768 Hz)
  * - HFXTCLK (external high-freq. osc., 1 - 48 MHz, only available in AM and LPM0)
  * - DCOCLK  (internal digitally controlled osc., max. 48 MHz)
  * - VLOCLK  (internal low-power osc., typ. 10 kHz)
  * - REFOCLK (internal low-power osc., typ. 32 or 128 kHz)
  * - MODCLK  (internal low-power osc., typ. 24 MHz, enabled only when required)
  * - SYSCLK  (internal osc., typ. 5 MHz, enabled only when required, e.g. for memory controller)
  *
  * default clock settings after a reset:
  * - MCLK = HSMCLK = SMCLK = DCOCLK
  * - ACLK = BCLK = LFXTCLK (if LFXT is available, otherwise or if osc fault is detected, REFOCLK is selected)
  * - SMCLK_EN, HSMCLK_EN, MCLK_EN and ACLK_EN bits are set
  *
  * clock requests:
  * - disable conditional clock requests by clearing the ACLK_EN, MCLK_EN, HSMCLK_EN, SMCLK_EN, VLO_EN, REFO_EN and MODOSC_EN bits in CSCLKEN
  */

  /*
   * simplest way to set 48 MHz clock using DCO:
   * PCM_setPowerState(PCM_AM_LDO_VCORE1);
   * CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_48);
   */

  MAP_GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_PJ, GPIO_PIN0 | GPIO_PIN1, GPIO_PRIMARY_MODULE_FUNCTION); // PJ.0 and PJ.1: LF crystal in and out
  MAP_GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_PJ, GPIO_PIN2 | GPIO_PIN3, GPIO_PRIMARY_MODULE_FUNCTION); // PJ.2 and PJ.3: HF crystal in and out

  MAP_CS_setReferenceOscillatorFrequency(CS_REFO_32KHZ);
  MAP_CS_setExternalClockSourceFrequency(LFXTCLK_SPEED, HFXTCLK_SPEED);  // this is necessary for CS_startHFXT() to work properly

  MAP_CS_setDCOFrequency(DCOCLK_SPEED);    // set DCO frequency (this function uses CS_setDCOCenteredFrequency and CS_tuneDCOFrequency and is not available on pre-release devices)
  MAP_CS_startLFXT(CS_LFXT_DRIVE0);     // enable LF crystal in non-bypass mode without a timeout (lowest current drain with drive level = 0)

  // enable the HF crystal:
  MAP_CS_startHFXT(0);            // start the HF crystal

  // select clock sources
  MAP_CS_initClockSignal(CS_MCLK, MCLK_SRC, MCLK_DIV);          // MCLK   = HFXTCLK
  MAP_CS_initClockSignal(CS_HSMCLK, HSMCLK_SRC, HSMCLK_DIV);    // HSMCLK = DCOCLK
  MAP_CS_initClockSignal(CS_SMCLK, SMCLK_SRC, SMCLK_DIV);       // SMCLK  
  MAP_CS_initClockSignal(CS_ACLK, ACLK_SRC, ACLK_DIV);          // ACLK   = LFXTCLK
  MAP_CS_initClockSignal(CS_BCLK, BCLK_SRC, BCLK_DIV);          // BCLK   = REFOCLk

  overflow_counter = 0;
  seconds = 0;
  MAP_SysTick_enableModule();
  MAP_SysTick_setPeriod(INTERVAL);
  //MAP_Interrupt_enableSleepOnIsrExit();
  MAP_SysTick_enableInterrupt();
  //MAP_Interrupt_enableMaster();
}

clock_time_t clock_time(void){
	//return (INTERVAL * overflow_counter + MAP_SysTick_getValue());

  clock_time_t t1, t2;
  do {
    t1 = overflow_counter;
    t2 = overflow_counter;
  } while(t1 != t2);
  return t1;
}

unsigned long clock_seconds(void){
	//return (INTERVAL * overflow_counter + MAP_SysTick_getValue()) / MAP_CS_getMCLK();

  return seconds;
}

void clock_set_seconds(unsigned long sec){
	//overflow_counter = INTERVAL * sec;
  seconds = sec;
}

void clock_wait(clock_time_t t){
  /*
	clock_time_t start_time = MAP_SysTick_getValue();
	while(MAP_SysTick_getValue() < (start_time + t));
  */
  clock_time_t start;

  start = clock_time();
  while(clock_time() - start < (clock_time_t)t);
}

void  clock_delay_usec(uint16_t dt){
  
	clock_time_t number_ticks = (MAP_CS_getMCLK() *dt) / 1000000;
    clock_time_t interval = INTERVAL;
	//clock_wait(number_ticks);
  clock_time_t start_time = MAP_SysTick_getValue();
  clock_time_t start_time_overflows = clock_time();
  clock_time_t current_overflows = start_time_overflows;
  clock_time_t target_overflows = current_overflows + ((start_time + number_ticks) / interval);
  clock_time_t target_ticks = (start_time + number_ticks) % interval;
  clock_time_t current_ticks = start_time;

  while(1){
      current_ticks = MAP_SysTick_getValue();
      current_overflows = clock_time();
      //printf("current time: %lu , start time: %lu , ticks: %u, interval: %lu \n", current_ticks, start_time, dt, interval);
      if((current_overflows > target_overflows) || (current_ticks >= target_ticks && current_overflows >= target_overflows)){
          break;
      }
  }

}

void SysTick_Handler(void){
	++overflow_counter;
  if(overflow_counter % CLOCK_CONF_SECOND == 0){
    ++seconds;
  }
    if (etimer_pending()) {
        etimer_request_poll();
    }
}
