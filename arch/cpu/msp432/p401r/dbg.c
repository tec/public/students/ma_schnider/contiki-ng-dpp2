/*---------------------------------------------------------------------------*/
//#include "contiki.h"
#include "dev/uart0-arch.h"
#include "stdio.h"
#include <string.h>
/*---------------------------------------------------------------------------*/
//#define SLIP_END     0300
//#undef putchar
/*---------------------------------------------------------------------------*/
int dbg_putchar(int c)
{
  uart0_write_byte((char)c);

  return c;
}
/*---------------------------------------------------------------------------*/

unsigned int dbg_send_bytes(const unsigned char *s, unsigned int len)
{
	unsigned int i = 0;

	while(s && *s != 0) {
		if(i >= len){
			break;
		}
		uart0_write_byte(*s++);
		i++;
	}

	return i;
}