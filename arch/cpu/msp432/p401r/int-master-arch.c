#include "contiki.h"
#include "sys/int-master.h"
#include "lib/msp432_driverlib_3_21_00_05/driverlib/MSP432P4xx/interrupt.h"
#include "rom_map.h"

static bool masterInterruptEnabled = false;

void int_master_enable(void){
	MAP_Interrupt_enableMaster();
	masterInterruptEnabled = true;
}

int_master_status_t int_master_read_and_disable(void){
	masterInterruptEnabled = false;
	return (int_master_status_t)MAP_Interrupt_disableMaster();
}

void int_master_status_set(int_master_status_t status){
	if(status == 0){
		masterInterruptEnabled = true; 
		MAP_Interrupt_enableMaster();
	} else {
		masterInterruptEnabled = false;
		MAP_Interrupt_disableMaster();
	}
}

bool int_master_is_enabled(void){
	return masterInterruptEnabled;
}
