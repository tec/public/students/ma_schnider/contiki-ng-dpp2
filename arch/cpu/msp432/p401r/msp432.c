#include "contiki.h"
#include "driverlib.h"
#include "uart0-arch.h"
#include "msp432-def.h"
#include <stdio.h>

void PSS_setLowSidePerformanceMode(uint_fast8_t ui8PowerMode)
{
#define SVSLLP_OFS    9

  PSS->KEY = PSS_KEY_VALUE;
  if (ui8PowerMode == PSS_FULL_PERFORMANCE_MODE)
  {
    BITBAND_PERI(PSS->CTL0, SVSLLP_OFS) = 0;
  } else
  {
    BITBAND_PERI(PSS->CTL0, SVSLLP_OFS) = 1;
  }
  PSS->KEY = 0;
}

void PSS_disableLowSide(void)
{
#define SVSLOFF_OFS   8
  PSS->KEY = PSS_KEY_VALUE;
  BITBAND_PERI(PSS->CTL0, SVSLOFF_OFS) = 1;
  PSS->KEY = 0;
}

void pcm_init(void){
  // clear LPM bits to unlock GPIO config
  PCM->CTL1 &= ~(PCM_CTL1_LOCKLPM5 | PCM_CTL1_LOCKBKUP);

  // increase core voltage level if necessary
  if (MCLK_SPEED > 24000000)
  {
    PCM_setCoreVoltageLevel(PCM_VCORE1);    // set VCore to 1 to support a frequency of 48 MHz (without timeout, waits until the new voltage has been established)
  } // else: VCORE0 (default setting after reset)
  
  SysCtl_enableSRAMBankRetention(SYSCTL_SRAM_BANK7);

  #if SVS_LOW_PERF
  // set low performance mode to save power in the low-power modes
  PSS_setHighSidePerformanceMode(PSS_NORMAL_PERFORMANCE_MODE);
  PSS_setLowSidePerformanceMode(PSS_NORMAL_PERFORMANCE_MODE);
#endif /* SVS_LOW_PERF */
#if !SVS_ENABLE
  PSS_disableHighSide();  // SVSHM
  PSS_disableLowSide(); // SVSHM
#endif /* !SVS_ENABLE */
}

void gpio_init(void)
{
    /*
     * notes:
     * - default for unused pins is output low
     * - for analog mode, set bits in SEL0 and SEL1 to 1
     */
    P1DIR  = 0xff & ~(BIT2 | BIT3 | BIT5 | BIT6 | BIT7);
    P1OUT  = 0;
    P1SEL0 = (BIT2 | BIT3 | BIT5 | BIT6 | BIT7); P1SEL1 = 0;
    P1REN  = 0; P1IFG  = 0;

    P2DIR  = 0xff;
    P2OUT  = 0;
    P2SEL0 = 0; P2SEL1 = 0;
    P2REN  = 0; P2IFG  = 0;

    P3DIR  = 0xff;
    P3OUT  = 0;
    P3SEL0 = 0; P3SEL1 = 0;
    P3REN  = 0; P3IFG  = 0;

    P4DIR  = 0xff & ~(BIT3 | BIT4 | BIT7);
    P4OUT  = 0;
    P4SEL0 = 0; P4SEL1 = 0;
    P4REN  = 0; P4IFG  = 0;

    P5DIR  = 0xff & ~(BIT5);
    P5OUT  = 0;
    P5SEL0 = BIT5; P5SEL1 = BIT5;
    P5REN  = 0; P5IFG  = 0;

    P6DIR  = 0xff & ~(BIT6 | BIT7);
    P6OUT  = 0;
    P6SEL0 = 0; P6SEL1 = (BIT6 | BIT7);
    P6REN  = 0; P6IFG  = 0;

    P7DIR  = 0xff & ~(BIT0 | BIT1 | BIT2);
    P7OUT  = 0;
    P7SEL0 = (BIT0 | BIT1 | BIT2); P7SEL1 = 0;
    P7REN  = 0;

    P8DIR  = 0xff & ~(BIT0 | BIT1);
    P8OUT  = 0;
    P8SEL0 = 0; P8SEL1 = 0;
    P8REN  = 0;

    P9DIR  = 0xff;
    P9OUT  = 0;
    P9SEL0 = 0; P9SEL1 = 0;
    P9REN  = 0;

    P10DIR  = 0xff;
    P10OUT  = 0;
    P10SEL0 = 0; P10SEL1 = 0;
    P10REN  = 0;

    PJDIR   = 0xff & ~(BIT4 | BIT5);
    PJOUT   = 0;
    PJSEL0 &= (BIT4 | BIT5);
    PJSEL1 &= (BIT4 | BIT5);

    /* custom mapping for pins on ports 2, 3 and 7 */
    PIN_MAP(SDCARD_SPI_SCK,  PM_UCA1CLK);
    PIN_MAP(SDCARD_SPI_MOSI, PM_UCA1SIMO);
    PIN_MAP(SDCARD_SPI_MISO, PM_UCA1SOMI);
}

void flash_init(void)
{
  /*
   * Flash Memory:
   * - the two banks 0 and 1 (each 128kB) can be accessed 'simultaneously', i.e. code execution
   *   from one bank can continue while program/erase is ongoing on the other bank
   * - wait states must be configured appropriately before changing the MCLK frequency, otherwise results are non-deterministic
   * - there are special wait states for other than 'normal' read modes (see p.18 in the datasheet for details)
   */

  // set the flash wait states for normal read mode based on the selected MCLK frequency (assume VCORE0 for up to 24 MHz)
  uint_fast8_t wait_states = 0;
  if (MCLK_SPEED > 24000000)    // VCORE1 mode
  {
    if (MCLK_SPEED > 32000000)
    {
      wait_states = 2;
    } else {
      wait_states = 1;
    }
  } else if (MCLK_SPEED > 12000000)
  {
    wait_states = 1;      // VCORE0
  }
  FlashCtl_setWaitState(FLASH_BANK0, wait_states);    // set flash memory wait states
  FlashCtl_setWaitState(FLASH_BANK1, wait_states);
}

/* starts the external low-frequency crystal and assigns it as the clock source for ACLK and BCLK */
void clock_start_lfxt(void)
{
  /* configure PJ.0 & PJ.1 for XT1 */
  PJSEL0 |= BIT0 | BIT1;
  PJSEL1 &= ~(BIT0 | BIT1);
  /* start LFXT in non-bypass mode without a timeout. */
  CS->KEY   = CS_KEY_VAL;
  CS->CTL1 &= ~CS_CTL1_SELA_7;
  CS->CTL1 |= CS_CTL1_SELA__LFXTCLK;    // source LFXTCLK to ACLK & BCLK
  CS->CTL2 &= ~CS_CTL2_LFXTDRIVE_3;     // lowest drive-strength
  CS->CTL2 |= CS_CTL2_LFXT_EN;
  while (CS->IFG & CS_IFG_LFXTIFG) CS->CLRIFG |= CS_IFG_LFXTIFG;  // wait for error flag to be cleared
  CS->KEY   = 0;
}

void spi_b_init(EUSCI_B_SPI_Type* spi_module, uint32_t sck_speed, uint_fast8_t cpol, uint_fast8_t cpha)
{
  // 3-wire SPI, MSB first, master mode, 8-bit character length
  spi_module->CTLW0 = UCSWRST | (UCMSB + UCMST + UCSYNC + (cpol ? UCCKPL : 0) + (cpha ? 0 : UCCKPH) + SPI_CLK_SRC);
  spi_module->BRW   = (uint16_t)(SPI_CLK_SPEED / sck_speed);
  spi_module->IE   &= ~(UCTXIE + UCRXIE);   /* make sure interrupts are disabled */
  spi_module->IFG   = 0;                    /* clear IFGs */
}

void i2c_init(EUSCI_B_Type* i2c_module, uint32_t scl_speed)
{
  /* master mode, 7-bit address */
  i2c_module->CTLW0 = UCSWRST | (UCMST | UCMODE_3 | I2C_CLK_SRC);
  i2c_module->BRW   = (uint16_t)(I2C_CLK_SPEED / scl_speed);
  i2c_module->IE    = 0;
}


uint_fast8_t i2c_read(EUSCI_B_Type* i2c_module, uint8_t slave_addr, uint16_t cmd, uint32_t num_bytes, uint8_t* out_data)
{
  I2C_ENABLE(i2c_module);
  i2c_module->I2CSA  = slave_addr;          /* set slave address (initiates master mode) */
  i2c_module->CTLW0 |= UCTR;                /* set to transmitter mode */
  i2c_module->CTLW0 |= UCTXSTT;             /* create START condition (write command = 0) */
  I2C_WAIT_START_CLR(i2c_module);           /* wait until address has been transmitted */
  clock_wait(MCLK_SPEED / I2C_CLK_SPEED);  /* wait for the ACK/NACK */
  if (i2c_module->IFG & UCNACKIFG)
  {
    // NACK received -> abort
    i2c_module->CTLW0 |= UCTXSTP;
    I2C_WAIT_STOP_CLR(i2c_module);
    I2C_DISABLE(i2c_module);
    return 0;
  }
  I2C_WRITE_BYTE(i2c_module, cmd >> 8);     /* write measurement command MSB */
  I2C_WRITE_BYTE(i2c_module, cmd & 0xff);   /* write measurement command LSB */
  I2C_WAIT_TXE(i2c_module);
  i2c_module->CTLW0 &= ~UCTR;               /* switch to receiver mode, send repeated START */
  i2c_module->CTLW0 |= UCTXSTT;             /* create START condition */

  while (num_bytes)
  {
    I2C_READ_BYTE(i2c_module, *out_data);
    out_data++;
    num_bytes--;
  }
  i2c_module->CTLW0 |= UCTXSTP;             /* generate STOP condition */
  I2C_WAIT_STOP_CLR(i2c_module);            /* wait for transmission to end, stop bit will be cleared automatically */
  I2C_CLR_RXBUF(i2c_module);

  I2C_DISABLE(i2c_module);

  return 1;
}


uint_fast8_t i2c_read_cmd8(EUSCI_B_Type* i2c_module, uint8_t slave_addr, uint8_t cmd, uint32_t num_bytes, uint8_t* out_data)
{
  I2C_ENABLE(i2c_module);
  i2c_module->I2CSA  = slave_addr;          /* set slave address (initiates master mode) */
  i2c_module->CTLW0 |= (UCTR | UCTXSTT);    /* set to transmitter mode and create START condition (write command = 0) */
  I2C_WAIT_START_CLR(i2c_module);           /* wait until address has been transmitted */
  clock_wait(MCLK_SPEED / I2C_CLK_SPEED);  /* wait for the ACK/NACK */
  if (i2c_module->IFG & UCNACKIFG)
  {
    // NACK received -> abort
    i2c_module->CTLW0 |= UCTXSTP;           /* STOP condition */
    I2C_WAIT_STOP_CLR(i2c_module);
    I2C_DISABLE(i2c_module);
    return 0;
  }
  I2C_WRITE_BYTE(i2c_module, cmd);          /* write command */
  I2C_WAIT_TXE(i2c_module);
  i2c_module->CTLW0 &= ~UCTR;               /* switch to receiver mode, send repeated START */
  i2c_module->CTLW0 |= UCTXSTT;             /* create START condition */

  while (num_bytes)
  {
    I2C_READ_BYTE(i2c_module, *out_data);
    out_data++;
    num_bytes--;
  }
  i2c_module->CTLW0 |= UCTXSTP;             /* generate STOP condition */
  I2C_WAIT_STOP_CLR(i2c_module);            /* wait for transmission to end, stop bit will be cleared automatically */
  I2C_CLR_RXBUF(i2c_module);

  I2C_DISABLE(i2c_module);

  return 1;
}

uint_fast8_t i2c_write(EUSCI_B_Type* i2c_module, uint8_t slave_addr, uint16_t cmd)
{
    I2C_ENABLE(i2c_module);
    i2c_module->I2CSA  = slave_addr;          /* set slave address (initiates master mode) */
    i2c_module->CTLW0 |= UCTR;                /* set to transmitter mode */
    i2c_module->CTLW0 |= UCTXSTT;             /* create START condition (write command = 0) */
    I2C_WAIT_START_CLR(i2c_module);           /* wait until address has been transmitted */

    clock_wait(MCLK_SPEED / I2C_CLK_SPEED);     /* wait for the ACK/NACK */
    if (i2c_module->IFG & UCNACKIFG)
    {
        // NACK received -> abort
        i2c_module->CTLW0 |= UCTXSTP;
        I2C_WAIT_STOP_CLR(i2c_module);
        I2C_DISABLE(i2c_module);
        return 0;
    }
    I2C_WRITE_BYTE(i2c_module, cmd >> 8);     /* write measurement command MSB */
    I2C_WRITE_BYTE(i2c_module, cmd & 0xff);   /* write measurement command LSB */
    I2C_WAIT_TXE(i2c_module);

    I2C_DISABLE(i2c_module);

    return 1;
}

uint_fast8_t i2c_write_cmd8(EUSCI_B_Type* i2c_module, uint8_t slave_addr, uint8_t cmd)
{
    I2C_ENABLE(i2c_module);
    i2c_module->I2CSA  = slave_addr;          /* set slave address (initiates master mode) */
    i2c_module->CTLW0 |= UCTR;                /* set to transmitter mode */
    i2c_module->CTLW0 |= UCTXSTT;             /* create START condition (write command = 0) */
    I2C_WAIT_START_CLR(i2c_module);           /* wait until address has been transmitted */
    clock_wait(MCLK_SPEED / I2C_CLK_SPEED);  /* wait for the ACK/NACK */
    if (i2c_module->IFG & UCNACKIFG)
    {
        // NACK received -> abort
        i2c_module->CTLW0 |= UCTXSTP;
        I2C_WAIT_STOP_CLR(i2c_module);
        I2C_DISABLE(i2c_module);
        return 0;
    }
    I2C_WRITE_BYTE(i2c_module, cmd);          /* write command */
    I2C_WAIT_TXE(i2c_module);

    I2C_DISABLE(i2c_module);

    return 1;
}

void msp432_init(void){

  WATCHDOG_STOP;

  gpio_init();    // set default configuration for all port pins

  pcm_init();
  flash_init();
  uart0_init();
}