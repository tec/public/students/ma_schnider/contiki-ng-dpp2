#include "rtimer-arch.h"
#include "driverlib.h"
#include "contiki.h"
#include "rom_map.h"


//2 32-bit timers available

void rtimer_arch_init(void){
	MAP_Timer32_initModule(TIMER32_0_BASE, TIMER32_PRESCALER, TIMER32_32BIT, TIMER32_FREE_RUN_MODE);
	MAP_Timer32_initModule(TIMER32_1_BASE, TIMER32_PRESCALER, TIMER32_32BIT, TIMER32_FREE_RUN_MODE);
	MAP_Timer32_enableInterrupt(TIMER32_1_BASE);
	MAP_Interrupt_enableInterrupt(INT_T32_INT2);
	MAP_Timer32_startTimer(TIMER32_0_BASE, false);
}

void rtimer_arch_schedule(rtimer_clock_t t){
	uint32_t clock_curr = rtimer_arch_now();
	if(t <= clock_curr){
		MAP_Timer32_setCount(TIMER32_1_BASE, clock_curr - t);
	}else {
		MAP_Timer32_setCount(TIMER32_1_BASE, (uint32_t)((uint64_t)((uint64_t)1 << 32) - t + clock_curr));
	}
	MAP_Timer32_startTimer(TIMER32_1_BASE, true);
}

rtimer_clock_t rtimer_arch_now(void){
	return MAP_Timer32_getValue(TIMER32_0_BASE);
}

void T32_INT2_IRQHandler(void){
	MAP_Timer32_clearInterruptFlag(TIMER32_1_BASE);
	rtimer_run_next();
}
