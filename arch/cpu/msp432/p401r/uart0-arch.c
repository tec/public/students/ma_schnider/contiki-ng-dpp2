#include "contiki.h"
#include "uart0-arch.h"
//#include "lib/msp432_driverlib_3_21_00_05/driverlib/MSP432P4xx/uart.h"
#include "driverlib.h"
//#include "msp432p401r.h"
#include "sys/process.h"
#include "stdbool.h"
#include "rom_map.h"

static bool initialized = false;
static volatile bool uart0_tx_in_progress = false;


void uart0_init(void){
	if(initialized){
		return;
	}

  //calculate uart parameters, can also be calculated here: http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP430BaudRateConverter/index.html
  float N = (float)UART_CLK_SPEED / (float)UART_BAUDRATE;
  uint_fast8_t  br_generation_mode = 0;
  uint_fast16_t br_div = 0;
  uint_fast16_t br_fmod = 0;
  uint_fast16_t br_smod = 0;
  uint_fast16_t frac = 0;

  // Table for UCxBRS (see Ref. Manual p.721, Table 22-4):
  const uint_fast16_t UCxBRS[] = {  0,  529,  715,  835, 1001, 1252, 1430, 1670, 2147, 2224, 2503, 3000, 3335, 3575, 3753, 4003, 4286, 4378, 5002, 5715, 6003, 6254, 6432, 6667, 7001, 7147, 7503, 7861, 8004, 8333, 8464, 8572, 8751, 9004, 9170, 9288,
                                 0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x11, 0x21, 0x22, 0x44, 0x25, 0x49, 0x4A, 0x52, 0x92, 0x53, 0x55, 0xAA, 0x6B, 0xAD, 0xB5, 0xB6, 0xD6, 0xB7, 0xBB, 0xDD, 0xED, 0xEE, 0xBF, 0xDF, 0xEF, 0xF7, 0xFB, 0xFD, 0xFE };
  if (N > 16)
  {
    br_div = (uint_fast16_t)(N / 16);
    br_fmod = (uint_fast16_t)(((N / 16.0f) - (float)br_div) * 16);
    br_generation_mode = 0x01;  // oversampling
  } else
  {
    br_div = (uint_fast16_t)N;
    br_generation_mode = 0x00;  // low-frequency
  }
  frac = (N - (uint_fast16_t)N) * 10000;   // fractional portion

  int_fast8_t i;
  for (i = 35; i >= 0; i--)
  {
    if (frac >= UCxBRS[i])
    {
      br_smod = UCxBRS[36 + i];
      break;
    }
  }

  const eUSCI_UART_Config uart0_config =
  {
	EUSCI_A_UART_CLOCKSOURCE_SMCLK,
	br_div,
	br_fmod,
	br_smod,
	EUSCI_A_UART_NO_PARITY,
	EUSCI_A_UART_LSB_FIRST,
	EUSCI_A_UART_ONE_STOP_BIT,
	EUSCI_A_UART_MODE,
	br_generation_mode
  };

  /* Selecting P1.2 and P1.3 in UART mode */
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1,
            GPIO_PIN1 | GPIO_PIN2 | GPIO_PIN3, GPIO_PRIMARY_MODULE_FUNCTION);

  MAP_UART_initModule(EUSCI_A0_BASE, &uart0_config);
  MAP_UART_enableModule(EUSCI_A0_BASE);
  //MAP_UART_registerInterrupt(EUSCI_A0_BASE, EUSCIA0_IRQHandler);
  //MAP_UART_enableInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);
  MAP_UART_enableInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_TRANSMIT_COMPLETE_INTERRUPT);
  MAP_Interrupt_enableInterrupt(INT_EUSCIA0);
  //MAP_Interrupt_enableSleepOnIsrExit();
  initialized = true;
  uart0_tx_in_progress = false;
}

void uart0_write_byte(uint_fast8_t byte){
	while(uart0_tx_in_progress == true ){
		//wait for tx finished

	}
    uart0_tx_in_progress = true;
	MAP_UART_transmitData(EUSCI_A0_BASE, byte);
}

void uart0_write(const char *buf, size_t buf_size){
	uint16_t i;
	for(i=0; i < buf_size; i++){
		uart0_write_byte(buf[i]);
	}
}



//uart 0 isr
void EUSCIA0_IRQHandler(void){
	//dont know what to do yet
	/*
	uint32_t status = MAP_UART_getEnabledInterruptStatus(EUSCI_A0_BASE);

    MAP_UART_clearInterruptFlag(EUSCI_A0_BASE, status);

    if(status & EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG)
    {

        MAP_UART_transmitData(EUSCI_A0_BASE, MAP_UART_receiveData(EUSCI_A0_BASE));
    }
    else if(status & EUSCI_A_UART_TRANSMIT_COMPLETE_INTERRUPT_FLAG){
        uart0_tx_in_progress = false;
    }
    */
    MAP_UART_clearInterruptFlag(EUSCI_A0_BASE, EUSCI_A_UART_TRANSMIT_COMPLETE_INTERRUPT_FLAG);
    uart0_tx_in_progress = false;
}
