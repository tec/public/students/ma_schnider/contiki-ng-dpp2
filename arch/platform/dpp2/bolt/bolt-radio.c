#include "dev/radio.h"
#include "contiki.h"
#include "driverlib.h"
#include "bolt.h"
#include "bolt-radio.h"
#include "stdio.h"
#include "net/packetbuf.h"
#include "net/netstack.h"

//TODO: add locks like in cc2420

static uint8_t volatile poll_mode = 0;

PROCESS(bolt_radio_process, "Bolt driver");
PROCESS(bolt_poll_process, "Bolt poll");

static uint8_t bolt_on;
static const void* tx_buffer;
static unsigned short tx_buffer_len;

static void set_poll_mode(uint8_t enable){
	poll_mode = enable;
	if(enable){
		MAP_GPIO_clearInterruptFlag(GPIO_PORT_P4, GPIO_PIN4);
		MAP_GPIO_disableInterrupt(GPIO_PORT_P4, GPIO_PIN4);
	} else{
		MAP_GPIO_enableInterrupt(GPIO_PORT_P4, GPIO_PIN4);
	}
}

static int bolt_radio_init(void){
	while(bolt_init() == 0){
		printf("bolt init failed, retry...\n");
	}
	bolt_flush();
	bolt_on = RADIO_POWER_MODE_ON;

	//enable IND line interrupt
	set_poll_mode(0);
	MAP_GPIO_enableInterrupt(GPIO_PORT_P4, GPIO_PIN4);
	MAP_Interrupt_enableInterrupt(INT_PORT4);
	process_start(&bolt_radio_process, NULL);
	process_start(&bolt_poll_process, NULL);
	return 1;
}

static int bolt_radio_prepare(const void *payload, unsigned short payload_len){
	if(payload == NULL || payload_len == 0){
		return RADIO_TX_ERR;
	}
	
	
	tx_buffer = payload;
	tx_buffer_len = payload_len;
	return RADIO_TX_OK;
}

static int bolt_radio_transmit(unsigned short transmit_len){
	if(bolt_status() == 0){
		//write queue is full
		return RADIO_TX_COLLISION;
	}
	if(bolt_write(tx_buffer, tx_buffer_len) == 1){
		return RADIO_TX_OK;
	}
	else{
		return RADIO_TX_ERR;
	}
}

static int bolt_radio_send(const void *payload, unsigned short payload_len){
	if(bolt_radio_prepare(payload, payload_len) == RADIO_TX_OK){
		return bolt_radio_transmit(payload_len);
	}
	else{
		return RADIO_TX_ERR;
	}
}

static int bolt_radio_read(void *buf, unsigned short buf_len){
	if(buf_len < BOLT_MAX_MSG_LEN){
		return 0;
	}
	else{
		return bolt_read(buf);
	}
}

//channel clear = bolt queue not full
static int bolt_radio_channel_clear(void){
	//clear channel = output queue not full
	return bolt_status();
}

//receiving packet = other side writing to bolt
static int bolt_radio_receiving_packet(void){
	//doesnt matter if other side is sending, sides are decoupled
	return 0;
}

//pending packet = something to read from bolt queue
static int bolt_radio_pending_packet(void){
	//if indication line is high, there is something to read
	return PIN_GET(BOLT_IND);

}

static int bolt_radio_on(void){
	//bolt_flush();
	bolt_on = RADIO_POWER_MODE_ON;
	MAP_GPIO_enableInterrupt(GPIO_PORT_P4, GPIO_PIN4);
	return 1;
}

static int bolt_radio_off(void){
	MAP_GPIO_disableInterrupt(GPIO_PORT_P4, GPIO_PIN4);
	bolt_on = RADIO_POWER_MODE_OFF;
	return 1;
}

static radio_result_t bolt_radio_get_value(radio_param_t param, radio_value_t *value){

	if(!value) {
    	return RADIO_RESULT_INVALID_VALUE;
  	}
  	switch(param){
  		case RADIO_PARAM_POWER_MODE:
  			*value = bolt_on;
  			return RADIO_RESULT_OK;
  		case RADIO_PARAM_RX_MODE:
  			*value = 0;
  			if(poll_mode){
  				*value |= RADIO_RX_MODE_POLL_MODE;
  			}
  			return RADIO_RESULT_OK;
  		case RADIO_CONST_MAX_PAYLOAD_LEN:
  			*value = BOLT_MAX_PAYLOAD_LEN;
  			return RADIO_RESULT_OK;
  		default:
    		return RADIO_RESULT_NOT_SUPPORTED;
  	}
}

static radio_result_t bolt_radio_set_value(radio_param_t param, radio_value_t value){

	switch(param) {
		case RADIO_PARAM_POWER_MODE:
			if(value == RADIO_POWER_MODE_ON){
				//BOLT is always on after init
				bolt_radio_on();
				return RADIO_RESULT_OK;
			}
			if(value == RADIO_POWER_MODE_OFF){
				//BOLT is always on after init
				bolt_radio_off();
				return RADIO_RESULT_OK;
			}
			return RADIO_RESULT_INVALID_VALUE;
		case RADIO_PARAM_CHANNEL:
			return RADIO_RESULT_NOT_SUPPORTED;
		case RADIO_PARAM_RX_MODE:
			if(value & ~(RADIO_RX_MODE_POLL_MODE)){
				printf("attempting to set invalid rx mode\n");
			}
			set_poll_mode((value & RADIO_RX_MODE_POLL_MODE) != 0);
			return RADIO_RESULT_OK;
		case RADIO_PARAM_TX_MODE:
			return RADIO_RESULT_NOT_SUPPORTED;
		case RADIO_PARAM_TXPOWER:
			return RADIO_RESULT_NOT_SUPPORTED;
		case RADIO_PARAM_CCA_THRESHOLD:
			return RADIO_RESULT_NOT_SUPPORTED;
		default:
			return RADIO_RESULT_NOT_SUPPORTED;
	}

}

static radio_result_t bolt_radio_get_object(radio_param_t param, void *dest, size_t size){
	return RADIO_RESULT_NOT_SUPPORTED;
}

static radio_result_t bolt_radio_set_object(radio_param_t param, const void *src, size_t size){
	return RADIO_RESULT_NOT_SUPPORTED;
}

const struct radio_driver bolt_driver =
  {
  	bolt_radio_init,
  	bolt_radio_prepare,
  	bolt_radio_transmit,
  	bolt_radio_send,
  	bolt_radio_read,
  	bolt_radio_channel_clear,
  	bolt_radio_receiving_packet,
  	bolt_radio_pending_packet,
  	bolt_radio_on,
  	bolt_radio_off,
  	bolt_radio_get_value,
  	bolt_radio_set_value,
  	bolt_radio_get_object,
  	bolt_radio_set_object
  };

  //this process reads messages from bolt and delivers them to the upper layers. Interrupt notifies process when there are messages in the queue
  PROCESS_THREAD(bolt_radio_process, ev, data){
  	int len;
  	PROCESS_BEGIN();
  	printf("bolt_radio_process started\n");
  	//try to fix: ind line already high
  	if(MAP_GPIO_getInputPinValue(GPIO_PORT_P4, GPIO_PIN4) != 0){
  	    process_poll(&bolt_radio_process);
  	}
  	while(1){
  		PROCESS_YIELD_UNTIL(!poll_mode && ev == PROCESS_EVENT_POLL);
  		//read all messages from the queue
  		do{
  			printf("bolt_radio receiver callback\n");

  			packetbuf_clear();
  			len = bolt_radio_read(packetbuf_dataptr(), PACKETBUF_SIZE);
  			if(len > 0){
  				packetbuf_set_datalen(len);
				NETSTACK_MAC.input();
			}
  		} while(len > 0);
  	}
  	PROCESS_END();
  }

//new data to read
void Bolt_ISR(void){
	process_poll(&bolt_radio_process);
    MAP_GPIO_clearInterruptFlag(GPIO_PORT_P4, GPIO_PIN4);
}

PROCESS_THREAD(bolt_poll_process, ev, data){
	static struct etimer poll_timer;
	PROCESS_BEGIN();
	etimer_set(&poll_timer, CLOCK_SECOND * BOLT_POLL_INTERVAL_S);

	while(1){
		if(MAP_GPIO_getInputPinValue(GPIO_PORT_P4, GPIO_PIN4) != 0){
			process_poll(&bolt_radio_process);
		}
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&poll_timer));
    	etimer_reset(&poll_timer);
	}

	PROCESS_END();
}