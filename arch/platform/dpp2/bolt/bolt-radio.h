#ifndef BOLT_RADIO_H_
#define BOLT_RADIO_H_

#include "contiki.h"

#define BOLT_POLL_INTERVAL_S		10

extern const struct radio_driver bolt_driver;
void Bolt_ISR(void);

#endif