#ifndef CONTIKI_CONF_H
#define CONTIKI_CONF_H

/*Include project specific conf */
#ifdef PROJECT_CONF_PATH
#include PROJECT_CONF_PATH
#endif //PROJECT_CONF_PATH

/*----------------------------------------------------------------------------*/

#include "dpp2-def.h"
#include "msp432-def.h"
#include "bolt.h"

/*----------------------------------------------------------------------------*/

// User configuration
#define UIP_CONF_TCP       						1
#define UIP_CONF_TCP_CONNS 						0
#define UIP_CONF_UDP							1
#define UIP_CONF_UDP_CONNS						2

//log levels
#define LOG_CONF_LEVEL_RPL						0
#define LOG_CONF_LEVEL_TCPIP						LOG_LEVEL_WARN
#define LOG_CONF_LEVEL_IPV6						LOG_LEVEL_WARN
#define LOG_CONF_LEVEL_6LOWPAN						LOG_LEVEL_INFO
#define LOG_CONF_LEVEL_NULLNET						0
#define LOG_CONF_LEVEL_MAC						LOG_LEVEL_INFO
#define LOG_CONF_LEVEL_FRAMER						LOG_LEVEL_INFO
#define LOG_CONF_LEVEL_COAP						LOG_LEVEL_INFO

#define UIP_CONF_BUFFER_SIZE						1280
//#define PACKETBUF_CONF_SIZE						1280
#define PACKETBUF_CONF_SIZE						128
//router for now, otherwise compilation error
#define UIP_CONF_ROUTER							1
//no router advertisement
#define UIP_CONF_ND6_SEND_RA					0
//no neighbor iscoverz	
#define UIP_CONF_ND6_SEND_NS					0
#define UIP_CONF_ND6_SEND_NA					0
#define NBR_TABLE_CONF_MAX_NEIGHBORS				1
#define NETSTACK_MAX_ROUTE_ENTRIES				1

//if setting this to 1, hardfault will occur
#define SICSLOWPAN_CONF_FRAG					1
#define SICSLOWPAN_CONF_COMPRESS_EXT_HDR		0	//dont compress udp header, scapy compatability problems
#define SICSLOWPAN_CONF_MAXAGE					1000
#define SICSLOWPAN_CONF_FRAGMENT_SIZE			(BOLT_MAX_PAYLOAD_LEN - 2 - 15) //synchronous transmissions and 802.15.4 headers
#define SICSLOWPAN_CONF_FRAGMENT_BUFFERS		8
#define SICSLOWPAN_CONF_REASS_CONTEXTS			6
#define QUEUEBUF_CONF_ENABLED					1
#define QUEUEBUF_CONF_NUM						8
#define IEEE802154_CONF_PANID					0xabcd

//CoAP
//#define COAP_MAX_CHUNK_SIZE					1024
//#define COAP_MAX_CHUNK_SIZE				256
#define COAP_MAX_CHUNK_SIZE						512

/* SICSLOWPAN_CONF_MAC_MAX_PAYLOAD is the maximum available size for
   frame headers, link layer security-related overhead,  as well as
   6LoWPAN payload. By default, SICSLOWPAN_CONF_MAC_MAX_PAYLOAD is
   127 bytes (MTU of 802.15.4) - 2 bytes (Footer of 802.15.4).
   and 5 byte LWB* overhead */
#define SICSLOWPAN_CONF_MAC_MAX_PAYLOAD 		((127 - 2) -5)
//no compression now for easier debugging. TODO change
//#define SICSLOWPAN_CONF_COMPRESSION 			SICSLOWPAN_COMPRESSION_IPV6
//TODO this does not compile, sicslowpan broken

//#define NETSTACK_CONF_WITH_IPV6					1
#define ROUTING_CONF_RPL_LITE						0
#define ROUTING_CONF_RPL_CLASSIC					0

//network drivers
/* Configure radio driver */
#ifndef NETSTACK_CONF_RADIO
#define NETSTACK_CONF_RADIO   						bolt_driver
#endif /* NETSTACK_CONF_RADIO */

#if MAC_CONF_WITH_OTHER
#define NETSTACK_CONF_MAC     						simplemac_driver
#endif

#ifndef NETSTACK_CONF_FRAMER
//#define NETSTACK_CONF_FRAMER 						no_framer
//#define NULLFRAMER_CONF_PARSE_802154				0
#endif /* NETSTACK_CONF_FRAMER */


/*----------------------------------------------------------------------------*/

// Include CPU related configuration
#include "msp432-conf.h"

/*----------------------------------------------------------------------------*/

#endif //CONTIKI_CONF_H
