#ifndef __CRC_H__
#define __CRC_H__

#include "stdint.h"

/*
 * P R O T O T Y P E S
 */

// CRC functions
uint8_t  crc8(const uint8_t* data, uint32_t num_bytes, uint8_t init_val);
uint8_t  crc8_table(const uint8_t* data, uint32_t num_bytes);
uint16_t crc16(const uint8_t* data, uint16_t num_bytes, uint16_t init_value);
uint16_t crc16_ccitt(const uint8_t* data, uint16_t num_bytes, uint16_t init_value);


#endif