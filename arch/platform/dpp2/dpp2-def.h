
/*
 * P I N   D E F I N I T I O N S
 */

/* when changing pin definitions, don't forget to adjust gpio_init() */

#define PIN_APP_BSL             PORT1, PIN0
#define PIN_APP_RXD             PORT1, PIN2
#define PIN_APP_TXD             PORT1, PIN3
#define PIN_APP_SCK             PORT1, PIN5
#define PIN_APP_MOSI            PORT1, PIN6
#define PIN_APP_MISO            PORT1, PIN7

#ifdef DPP_REV1
#define PIN_APP_LED1            PORT2, PIN4
#define PIN_APP_LED2            PORT3, PIN0
#else /* DPP_REV1 */
#define PIN_APP_LED1            PORT2, PIN0
#define PIN_APP_LED2            PORT2, PIN1
#endif /* DPP_REV1 */
#define PIN_APP_GPIO1           PORT2, PIN2
#define PIN_APP_GPIO2           PORT2, PIN3

#define PIN_APP_EXT0            PORT3, PIN0
#define PIN_APP_EXT1            PORT3, PIN1
#define PIN_APP_EXT2            PORT3, PIN2
#define PIN_APP_EXT3            PORT3, PIN3
#define PIN_APP_EXT4            PORT3, PIN4
#define PIN_APP_EXT5            PORT3, PIN5
#define PIN_APP_EXT6            PORT3, PIN6
#define PIN_APP_EXT7            PORT3, PIN7

#define PIN_COM_TREQ            PORT4, PIN2
#ifdef DPP_REV1
#define PIN_APP_ACK             PORT1, PIN4
#define PIN_APP_IND             PORT1, PIN1
#define PIN_COM_IND             PORT10, PIN1
#define PIN_APP_MODE            PORT10, PIN2
#define PIN_APP_REQ             PORT10, PIN3
#else /* DPP_REV1 */
#define PIN_COM_IND             PORT4, PIN3
#define PIN_APP_IND             PORT4, PIN4
#define PIN_APP_MODE            PORT4, PIN5
#define PIN_APP_REQ             PORT4, PIN6
#define PIN_APP_ACK             PORT4, PIN7
#endif /* DPP_REV1 */

#define PIN_APP_EXT13           PORT5, PIN0
#define PIN_APP_EXT12           PORT5, PIN1
#define PIN_APP_EXT11           PORT5, PIN2
#define PIN_APP_EXT10           PORT5, PIN3
#define PIN_APP_EXT9            PORT5, PIN6
#define PIN_APP_EXT8            PORT5, PIN7

#define PIN_APP_VBAT            PORT5, PIN5

#define PIN_APP_SD0             PORT7, PIN0
#define PIN_APP_SD1             PORT7, PIN1
#define PIN_APP_SD2             PORT7, PIN2
#define PIN_APP_SD3             PORT7, PIN3

#define PIN_APP_SENSOR_SDA      PORT6, PIN6
#define PIN_APP_SENSOR_SCL      PORT6, PIN7

#define PIN_APP_COM_EN          PORT8, PIN0
#define PIN_APP_PERIPH_EN       PORT8, PIN1

/*
 * M A C R O S
 */

#define PWRSW_ON(p)       PIN_CFG_OUT_I(p)    /* output low = on */
#define PWRSW_OFF(p)      PIN_CFG_IN_I(p)     /* high impedance = off */

/*
 * P I N   M A P P I N G
 */

/* when changing pin mappings, don't forget to adjust gpio_init() */

/* Debug UART */
#define DBG_UART_RXD            PIN_APP_RXD
#define DBG_UART_TXD            PIN_APP_TXD

/* Debug pins */
#define DBG_PIN1                PIN_APP_GPIO1
#define DBG_PIN2                PIN_APP_GPIO2

/* LEDs */
#define LED_STATUS              PIN_APP_LED1
#define LED_ERROR               PIN_APP_LED2

/* Power Switch */
#define PWRSW_COM               PIN_APP_COM_EN
#define PWRSW_PERIPH            PIN_APP_PERIPH_EN

/* I2C Sensors */
#define SENSOR_SDA              PORT6, PIN6
#define SENSOR_SCL              PORT6, PIN7

/* Voltage monitor */
#define VIN_MON                 PIN_APP_VBAT

/* SD Card */
#define SDCARD_SPI_CS           PIN_APP_SD3
#define SDCARD_SPI_MOSI         PIN_APP_SD2
#define SDCARD_SPI_MISO         PIN_APP_SD0
#define SDCARD_SPI_SCK          PIN_APP_SD1

/* BOLT */
#define BOLT_REQ                PIN_APP_REQ
#define BOLT_ACK                PIN_APP_ACK
#define BOLT_MODE               PIN_APP_MODE
#define BOLT_IND                PIN_APP_IND
#define BOLT_IND_OUT            PIN_COM_IND
#define BOLT_TREQ               PIN_COM_TREQ
#define BOLT_SPI_MOSI           PIN_APP_MOSI
#define BOLT_SPI_MISO           PIN_APP_MISO
#define BOLT_SPI_SCK            PIN_APP_SCK
#define BOLT_IND_PIN			4
#define BOLT_IND_PORT			4

/*LED*/
#define LEDS_CONF_COUNT			2
#define LED_1_PORT_NUMBER		2
#define LED_1_PIN_NUMBER		0
#define LED_2_PORT_NUMBER		2
#define LED_2_PIN_NUMBER		1

//SPI
#define SPI_CONF_CONTROLLER_COUNT	1

/* I2C */
#define I2C_SCL_SPEED           100000
#define I2C_CLK_SRC             EUSCI_B_CTLW0_SSEL__SMCLK
#define I2C_CLK_SPEED           SMCLK_SPEED
#define I2C_MODULE              EUSCI_B3                // default module for I2C sensors
#define I2C_MODULE_ADDR         EUSCI_B3_BASE           // default module for I2C sensors

/* I2C */
#define I2C_WAIT_TXE(module)            while(!((module)->IFG & UCTXIFG0))
#define I2C_WRITE_BYTE(module, b)       I2C_WAIT_TXE(module); (module)->TXBUF = (b)
#define I2C_WAIT_RXNE(module)           while(!((module)->IFG & UCRXIFG0))
#define I2C_WAIT_START_CLR(module)      while((module)->CTLW0 & UCTXSTT)    // note: flag is cleared before the ACK/NACK has been received!
#define I2C_WAIT_STOP_CLR(module)       while((module)->CTLW0 & UCTXSTP)
#define I2C_WAIT_BUSY(module)           while ((module)->STATW & UCBUSY)
#define I2C_READ_BYTE(module, b)        I2C_WAIT_RXNE(module); (b) = (module)->RXBUF
#define I2C_CLR_RXBUF(module)           (void)(module)->RXBUF
#define I2C_ENABLE(module)              ((module)->CTLW0 &= ~UCSWRST)
#define I2C_DISABLE(module)             ((module)->CTLW0 |= UCSWRST)

/* peripherals */
#define PERIPH_ENABLE			1
