#include "contiki.h"
#include "dev/leds.h"
#include "dpp2-def.h"

/* old code
#define LED_1_PIN_NUMBER	8
#define LED_2_PIN_NUMBER	9
*/

//const leds_t led1 = {.pin=LED_1_PIN_NUMBER, .negative_logic=false};
//const leds_t led2 = {.pin=LED_2_PIN_NUMBER, .negative_logic=false};

//const leds_t leds_arch_leds[2] = {led1, led2};

const leds_t leds_arch_leds[LEDS_CONF_COUNT] = {
	{.pin=LED_1_PIN_NUMBER, .port=LED_1_PORT_NUMBER, .negative_logic=false},
	{.pin=LED_2_PIN_NUMBER, .port=LED_2_PORT_NUMBER, .negative_logic=false}
};
