#include "contiki.h"
#include "dpp2-def.h"
//#include "dev/uart0-arch.h"
#include "sys/int-master.h"
#include "driverlib.h"
#include "lib/sensors.h"
#include "dev/leds.h"
#include "contiki-net.h"
#include "node-id.h"

/* Log configuration */
#include "sys/log.h"
#define LOG_MODULE "APP"
#define LOG_LEVEL LOG_LEVEL_MAIN


//this is needed to work with flocklab
uint16_t FLOCKLAB_NODE_ID = NODEID;
const char NODE_PREFIX[] = "fc::0";


static void
set_lladdr(void)
{
	linkaddr_t node_link_addr;
	//set u bit to 1 (local), use company_id 0, NODE_ID in range 0-255
	node_link_addr.u8[0] = 0x02;
	node_link_addr.u8[1] = 0;
	node_link_addr.u8[2] = 0;
	node_link_addr.u8[3] = 0;
	node_link_addr.u8[4] = 0;
	node_link_addr.u8[5] = 0;
	node_link_addr.u8[6] = 0;
	node_link_addr.u8[7] = FLOCKLAB_NODE_ID;
	linkaddr_set_node_addr(&node_link_addr);
}




void platform_init_stage_one(void){
	msp432_init();
	gpio_hal_init();
	leds_init();
	int_master_enable();
	//leds_single_on(LEDS_LED1);
	
}


void platform_init_stage_two(void){
	set_lladdr();
	//leds_single_on(LEDS_LED2);
}

void platform_init_stage_three(void){
	//Interrupt_enableMaster();
	//leds_single_off(LEDS_LED1);
	#if PERIPH_ENABLE
        i2c_init(I2C_MODULE, I2C_SCL_SPEED);
		PWRSW_ON(PWRSW_PERIPH);
	#endif
	uip_ipaddr_t ipaddr;
	uiplib_ip6addrconv(NODE_PREFIX, &ipaddr);
	uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
	uip_ds6_addr_add(&ipaddr, 0, ADDR_MANUAL);

	LOG_INFO("Added global IPv6 address ");
  LOG_INFO_6ADDR(&ipaddr);
  LOG_INFO_("\n");
  uip_ds6_defrt_add(&ipaddr, 0);
}

void platform_idle(void){
	//go to low power state
}
