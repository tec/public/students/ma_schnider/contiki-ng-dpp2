#include "arch/dev/bme280/bme280-arch.h"
#include "driverlib.h"
#include "stdlib.h"


#define SENSOR_I2C_INTERFACE    EUSCI_B3_BASE
#define SENSOR_I2C_TIMEOUT      ((uint32_t) 10000)
#define I2C_MODULE 				EUSCI_B3

#define SENSOR_I2C_SCL_PORT     GPIO_PORT_P6
#define SENSOR_I2C_SCL_PIN      GPIO_PIN7
#define SENSOR_I2C_SDA_PORT     GPIO_PORT_P6
#define SENSOR_I2C_SDA_PIN      GPIO_PIN6
#define SENSOR_I2C_GPIO_AF      GPIO_SECONDARY_MODULE_FUNCTION

/**
 * Sensor I2C interface configuration (with clock source frequency placeholder)
 */
eUSCI_I2C_MasterConfig sensor_i2cConfig = {
  .selectClockSource = EUSCI_B_I2C_CLOCKSOURCE_SMCLK,
  .i2cClk = 0,
  .dataRate = EUSCI_B_I2C_SET_DATA_RATE_400KBPS,
  .byteCounterThreshold = 0,
  .autoSTOPGeneration = EUSCI_B_I2C_NO_AUTO_STOP,
};

//initialize i2c module
void bme280_arch_i2c_init(void){
	// initialize GPIO first
  GPIO_setAsPeripheralModuleFunctionInputPin(SENSOR_I2C_SCL_PORT, SENSOR_I2C_SCL_PIN, SENSOR_I2C_GPIO_AF);
  GPIO_setAsPeripheralModuleFunctionInputPin(SENSOR_I2C_SDA_PORT, SENSOR_I2C_SDA_PIN, SENSOR_I2C_GPIO_AF);

  // I2C master configuration
  sensor_i2cConfig.i2cClk = CS_getSMCLK();
  I2C_initMaster(SENSOR_I2C_INTERFACE, &sensor_i2cConfig);

  // enable interface
  I2C_enableModule(SENSOR_I2C_INTERFACE);
}

//i2c read registers
void bme280_arch_i2c_read_mem(uint8_t addr, uint8_t reg, uint8_t *buf, uint8_t bytes){
	/*
   * Data on the bus should be like
   * |------------+---------------------|
   * | I2C action | Data                |
   * |------------+---------------------|
   * | Start      | [device_address]    |
   * | Write      | (register_address)  |
   * | Start      | [device_address]    |
   * | Read       | (data[0])           |
   * | Read       | (....)              |
   * | Read       | (data[bytes - 1])   |
   * | Stop       | -                   |
   * |------------+---------------------|
   *
   * return 0 for success, non-zero for failure
   */

  // wait for I2C to be ready to read
  while (I2C_isBusBusy(SENSOR_I2C_INTERFACE)) {
    continue;
  }

  // configure slave address
  I2C_setSlaveAddress(SENSOR_I2C_INTERFACE, addr);

  // initiate read with START and writing register address
  I2C_masterSendMultiByteStart(SENSOR_I2C_INTERFACE, reg);

  // wait for transmission to complete, clear interrupt flag
  while(!((I2C_MODULE)->IFG & UCTXIFG0));
  /*while (!I2C_getInterruptStatus(SENSOR_I2C_INTERFACE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0)) {
    continue;
  }*/
  I2C_clearInterruptFlag(SENSOR_I2C_INTERFACE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0);

  // check for NACK, on NACK stop and exit with failure
  if (I2C_getInterruptStatus(SENSOR_I2C_INTERFACE, EUSCI_B_I2C_NAK_INTERRUPT)) {
    I2C_masterSendMultiByteStop(SENSOR_I2C_INTERFACE);
    return;
  }

  // start receive transaction with RE-START
  I2C_masterReceiveStart(SENSOR_I2C_INTERFACE);

  // wait for RE-START to complete send
  while(I2C_masterIsStartSent(SENSOR_I2C_INTERFACE)) {
    continue;
  }

  // receive all but last data byte
  while (bytes > 1) {
    // wait for next byte transmission to complete
    while (!I2C_getInterruptStatus(SENSOR_I2C_INTERFACE, EUSCI_B_I2C_RECEIVE_INTERRUPT0)) {
      continue;
    }

    // read received byte, clears RX interrupt automatically
    *buf = I2C_masterReceiveMultiByteNext(SENSOR_I2C_INTERFACE);
    buf = buf + 1;

    bytes = bytes - 1;
  }

  // --- broken driverlib implementation to handle reception of last byte and STOPing ---
  // *buf = I2C_masterReceiveMultiByteFinish(SENSOR_I2C_INTERFACE);
  // return 0;
  // --- alternative handling using driverlib API below ---

  // receive last byte and generate stop condition
  I2C_masterReceiveMultiByteStop(SENSOR_I2C_INTERFACE);

  // wait for stop interrupt
  while (!I2C_getInterruptStatus(SENSOR_I2C_INTERFACE, EUSCI_B_I2C_STOP_INTERRUPT)) {
    continue;
  }

  // wait for next byte transmission to complete
  while (!I2C_getInterruptStatus(SENSOR_I2C_INTERFACE, EUSCI_B_I2C_RECEIVE_INTERRUPT0)) {
    continue;
  }

  // actually read last reveiced value
  *buf = I2C_masterReceiveMultiByteNext(SENSOR_I2C_INTERFACE);
  return;
}

//i2c write to a single register
void bme280_arch_i2c_write_mem(uint8_t addr, uint8_t reg, uint8_t value){
	/*
   * Data on the bus should be like
   * |------------+---------------------|
   * | I2C action | Data                |
   * |------------+---------------------|
   * | Start      | [device_address]    |
   * | Write      | (register_address)  |
   * | Write      | (data[0])           |
   * | Write      | (...)               |
   * | Write      | (data[count - 1])   |
   * | Stop       | -                   |
   * |------------+---------------------|
   *
   * return 0 for success, non-zero for failure
   */

  // wait for I2C to be ready to write
  while (I2C_isBusBusy(SENSOR_I2C_INTERFACE)) {
    continue;
  }

  // configure slave address
  I2C_setSlaveAddress(SENSOR_I2C_INTERFACE, addr);

  // initiate read with START and writing register address
  I2C_masterSendMultiByteStart(SENSOR_I2C_INTERFACE, reg);

  // wait for transmission to complete
  while (!I2C_getInterruptStatus(SENSOR_I2C_INTERFACE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0)) {
    continue;
  }

  // check for NACK, on NACK stop and exit with failure
  if (I2C_getInterruptStatus(SENSOR_I2C_INTERFACE, EUSCI_B_I2C_NAK_INTERRUPT)) {
    I2C_masterSendMultiByteStop(SENSOR_I2C_INTERFACE);
    return;
  }

  
  while (!I2C_getInterruptStatus(SENSOR_I2C_INTERFACE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0)) {
      continue;
  }

  // send next byte, clears TX interrupt automatically
  I2C_masterSendMultiByteNext(SENSOR_I2C_INTERFACE, value);

  // wait for last transmission to complete, send STOP and finally clear flag
  while (!I2C_getInterruptStatus(SENSOR_I2C_INTERFACE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0)) {
    continue;
  }
  I2C_masterSendMultiByteStop(SENSOR_I2C_INTERFACE);
  I2C_clearInterruptFlag(SENSOR_I2C_INTERFACE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0);

  return;
}