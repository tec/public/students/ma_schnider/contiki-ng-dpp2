#include "simplemac/simplemac.h"
#include "net/mac/mac-sequence.h"
#include "net/packetbuf.h"
#include "net/netstack.h"
#include "lib/random.h"

/*
SimpleMAC just forwards packets between the radio and the network layer, respecting 802.15.4
*/

//log config
#include "sys/log.h"
#define LOG_MODULE "SimpleMAC"
#define LOG_LEVEL LOG_LEVEL_MAC

static int simplemac_on(void){
	return NETSTACK_RADIO.on();
}

static int simplemac_off(void){
	return NETSTACK_RADIO.off();
}

static void simplemac_init(void){
	simplemac_on();
}

static void simplemac_send_packet(mac_callback_t sent, void *ptr){
	static uint8_t initialized = 0;
	static uint8_t seqno;
	int ret;

	if(!initialized){
		/* Initialize the sequence number to a random value as per 802.15.4. */
		initialized = 1;
		seqno = random_rand();
	}
	if(seqno == 0) {
    	/* PACKETBUF_ATTR_MAC_SEQNO cannot be zero, due to a pecuilarity
       in framer-802154.c. */
    	seqno++;
  	}
  	packetbuf_set_attr(PACKETBUF_ATTR_MAC_SEQNO, seqno++);
  	packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, FRAME802154_DATAFRAME);
  	packetbuf_set_addr(PACKETBUF_ADDR_SENDER, &linkaddr_node_addr);
  	packetbuf_set_attr(PACKETBUF_ATTR_MAC_ACK, 0);
  	if(NETSTACK_FRAMER.create() < 0){
  		/* Failed to allocate space for headers */
    	LOG_ERR("failed to create packet\n");
    	ret = MAC_TX_ERR_FATAL;
  	} else {
  		switch(NETSTACK_RADIO.send(packetbuf_hdrptr(), packetbuf_totlen())){
  			case RADIO_TX_OK:
  				ret = MAC_TX_OK;
  				break;
  			case RADIO_TX_COLLISION:
  				ret = MAC_TX_COLLISION;
  				break;
  			case RADIO_TX_ERR:
  				ret = MAC_TX_ERR;
  				break;
  			default:
  				ret = MAC_TX_ERR;
  				break;
  		}
  	}
  	mac_call_sent_callback(sent, ptr, ret, 1);
}

static void simplemac_input_packet(void){
	//this for debugging
	/*
	uint16_t datalen = packetbuf_datalen();
	uint8_t* dataptr = packetbuf_dataptr();
	printf("received data: ");
	for(uint16_t i=0; i<datalen; i++){
		printf(" %u ", *dataptr);
		dataptr++;
	}
	printf(" \n");
	 */
	//debugging end

	if(NETSTACK_FRAMER.parse() < 0){
		LOG_ERR("failed to parse %u\n", packetbuf_datalen());
	} else if(!linkaddr_cmp(packetbuf_addr(PACKETBUF_ADDR_RECEIVER), &linkaddr_node_addr) && !packetbuf_holds_broadcast()){
		LOG_WARN("packet dest not for us\n");
	} else {
		int duplicate = 0;
		duplicate = mac_sequence_is_duplicate();
		if(duplicate){
			//drop packet
			LOG_WARN("drop duplicate link layer packet\n");
		}
		else{
			mac_sequence_register_seqno();
		}
		if(!duplicate){
			LOG_INFO("received packet from ");
	    	LOG_INFO_LLADDR(packetbuf_addr(PACKETBUF_ADDR_SENDER));
	    	LOG_INFO_(", seqno %u, len %u\n", packetbuf_attr(PACKETBUF_ATTR_MAC_SEQNO), packetbuf_datalen());

	    	NETSTACK_NETWORK.input();
	    }
	}
}

static int simplemac_max_payload(void){
	int framer_hdrlen;
	radio_value_t max_radio_payload_len;
	radio_result_t res;

	framer_hdrlen = NETSTACK_FRAMER.length();
	res = NETSTACK_RADIO.get_value(RADIO_CONST_MAX_PAYLOAD_LEN,
                                 &max_radio_payload_len);

	if(res == RADIO_RESULT_NOT_SUPPORTED) {
    	LOG_ERR("Failed to retrieve max radio driver payload length\n");
    	return 0;
  	}

  	if(framer_hdrlen < 0) {
    	/* Framing failed, we assume the maximum header length */
    	framer_hdrlen = SIMPLEMAC_MAX_HEADER;
  	}

  	return MIN(max_radio_payload_len, PACKETBUF_SIZE) - framer_hdrlen;
}

const struct mac_driver simplemac_driver = {
	"SimpleMAC",
	simplemac_init,
	simplemac_send_packet,
	simplemac_input_packet,
	simplemac_on,
	simplemac_off,
	simplemac_max_payload,
};