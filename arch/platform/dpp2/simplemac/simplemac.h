#ifndef SIMPLEMAC_H_
#define SIMPLEMAC_H_

#include "contiki.h"
#include "net/mac/mac.h"

//a default max len for lower layer headers
#define SIMPLEMAC_MAX_HEADER		21;

extern const struct mac_driver simplemac_driver;


#endif