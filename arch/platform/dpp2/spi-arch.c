#include "dev/spi.h"
#include "stdbool.h"
//#include "spi.h"
#include "driverlib.h"
#include "contiki.h"
#include "rom_map"

static bool spi_initialized = false;

typedef struct spi_locks_s {
	mutex_t lock;
	const spi_device_t *owner;
} spi_locks_t;

//one lock per SPI controller
spi_locks_t board_spi_locks_spi[SPI_CONTROLLER_COUNT] = { { MUTEX_STATUS_UNLOCKED, NULL } };

/* SPI Master Configuration Parameter */
const eUSCI_SPI_MasterConfig spiMasterConfig =
{
        EUSCI_B_SPI_CLOCKSOURCE_SMCLK,             // SMCLK Clock Source
        SMCLK_SPEED,                                   // SMCLK  3MHZ
        BOLT_SPI_SPEED,                                    // SPICLK = SMCLK/2
        EUSCI_B_SPI_MSB_FIRST,                     // MSB First
        0,    // Phase 0
        0, // polarity 0
        EUSCI_B_SPI_3PIN                           // 3Wire SPI Mode
};

//need to first call init function
void spi_arch_init(void){
	//configure bolt spi interface
	MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1, GPIO_PIN5 | GPIO_PIN6 | GPIO_PIN7, GPIO_PRIMARY_MODULE_FUNCTION);
	MAP_SPI_initMaster(EUSCI_B0_BASE, &spiMasterConfig);
	MAP_SPI_enableModule(EUSCI_B0_BASE);

	//possibly enable interrupts here

	MAP_SPI_clearInterruptFlag(EUSCI_B0_BASE,EUSCI_B_SPI_TRANSMIT_INTERRUPT | EUSCI_SPI_RECEIVE_INTERRUPT);
	spi_initialized = true;
}

void spi_arch_deinit(void){
	MAP_SPI_disableModule(EUSCI_B0_BASE);

	//possibly disable interrupts here

	MAP_SPI_clearInterruptFlag(EUSCI_B0_BASE,EUSCI_B_SPI_TRANSMIT_INTERRUPT | EUSCI_SPI_RECEIVE_INTERRUPT);
	spi_initialized = false;
}

bool spi_arch_has_lock(const spi_device_t *dev){
	if(board_spi_locks_spi[dev->spi_controller].owner == dev){
		return true;
	}

	return false;
}

bool spi_arch_is_bus_locked(const spi_device_t *dev){
	if(board_spi_locks_spi[dev->spi_controller].lock == MUTEX_STATUS_LOCKED){
		return true;
	}

	return false;
}

spi_status_t spi_arch_lock_and_open(const spi_device_t *dev){
	if(mutex_try_lock(&board_spi_locks_spi[dev->spi_controller].lock) == false){
		return SPI_DEV_STATUS_BUS_LOCKED;
	}
	board_spi_locks_spi[dev->spi_controller].owner = dev;
	if(!spi_initialized){
		spi_arch_init();
	}
}

spi_status_t spi_arch_close_and_unlock(const spi_device_t *dev){
	if(!spi_arch_has_lock(dev)){
		return SPI_DEV_STATUS_BUS_NOT_OWNED;
	}

	spi_arch_deinit();
	board_spi_locks_spi[dev->spi_controller].owner = NULL;
	mutex_unlock(&board_spi_locks_spi[dev->spi_controller].lock);
	return SPI_DEV_STATUS_OK;
}

/* Assumes that checking dev and bus is not NULL before calling this */
spi_status_t spi_arch_transfer(const spi_device_t *dev, const uint8_t *data, int wlen, uint8_t *buf, int rlen, int ignore_len){
	int i,totlen;
	uint8_t c;

	if(!spi_arch_has_lock(dev)){
		return SPI_DEV_STATUS_BUS_NOT_OWNED;
	}
	LOG_DBG("SPI: transfer (r:%d,w:%d) ", rlen, wlen);

	if(write_buf == NULL && wlen > 0){
		return SPI_DEV_STATUS_EINVAL;
	}
	if(inbuf == NULL && rlen > 0){
		return SPI_DEV_STATUS_EINVAL;
	}

	totlen = MAX(rlen + ignore_len, wlen);
	if(totlen == 0) {
       /* Nothing to do */
       return SPI_DEV_STATUS_OK;
    }

    
    for(i=0; i < totlen; i++){
    	/* Polling to see if the TX buffer is ready */
    	while (!(MAP_SPI_getInterruptStatus(EUSCI_B0_BASE,EUSCI_B_SPI_TRANSMIT_INTERRUPT)));
    	MAP_SPI_clearInterruptFlag(EUSCI_B0_BASE,EUSCI_B_SPI_TRANSMIT_INTERRUPT);
    	if(i < wlen){
    		c = write_buf[i];
    	}
    	else{
    		c = 0;
    	}
    	//send data
    	MAP_SPI_transmitData(EUSCI_B0_BASE, c);
    	//wait receive finished
    	while (!(MAP_SPI_getInterruptStatus(EUSCI_B0_BASE,EUSCI_SPI_RECEIVE_INTERRUPT)));
    	MAP_SPI_clearInterruptFlag(EUSCI_B0_BASE,EUSCI_SPI_RECEIVE_INTERRUPT);
    	c = MAP_SPI_receiveData(EUSCI_B0_BASE);
    	if(i < rlen){
    		inbuf[i] = c;
    	}
    }

    return SPI_DEV_STATUS_OK;
}
