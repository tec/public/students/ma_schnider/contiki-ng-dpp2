#include "contiki.h"
#include "coap-engine.h"

#include "sys/log.h"
#define LOG_MODULE "CoAP"
#define LOG_LEVEL LOG_LEVEL_CoAP

static void td_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
/* not working, memory issue probably */
/*
static const char thingDescription[] = "{\
   \"@context\": [\
      \"https://www.w3.org/2019/wot/td/v1\",\
      {\
      	\"cov\": \"http://www.example.org/coap-binding#\",\
      	\"om\": \"http://www.ontology-of-units-of-measure.org/resource/om-2/\"\
      }\
    ],\
    \"properties\": {\
        \"temperature\": {\
            \"type\": \"int\",\
            \"minimum\": -40,\
            \"maximum\": 125,\
            \"unit\": \"om:degree_Celsius\",\
            \"forms\": [{\
                \"op\": \"readproperty\",\
                \"href\": \"coaps://mynode.example.com/heating\",\
                \"cov:methodName\" : \"GET\"\
            }]\
        }\
    },\
    \"actions\": {\
        \"heating\": {\
            \"type\": \"string\",\
            \"enum\": [\"on\", \"off\"],\
            \"forms\": [{\
                \"href\": \"coaps://mynode.example.com/heating\",\
                \"cov:methodName\" : \"POST\"\
            }]\
        }\
    },\
    \"events\": {\
        \"temperature_threshold\": {\
        	\"type\": \"int\",\
        	\"forms\": [{\
                \"href\": \"coaps://mynode.example.com/temp_obs\",\
                \"cov:methodName\" : \"GET\",\
                \"subprotocol\" : \"cov:observe\"\
            }]\
        }\
    }\
}";*/

static const char thingDescription_temp[] = "{\
\"properties\": {\
\"temperature\": {\
\"type\": \"int\",\
\"minimum\": -40,\
\"maximum\": 125,\
\"unit\": \"om:degree_Celsius\",\
\"forms\": [{\
\"op\": \"readproperty\",\
\"href\": \"coaps://[fc::1]/temp\",\
\"cov:methodName\" : \"GET\"\
}]\
}\
},\
\"events\": {\
\"temperature_threshold\": {\
\"type\": \"int\",\
\"forms\": [{\
\"href\": \"coap://[fc::1]/temp_obs\",\
\"cov:methodName\" : \"GET\",\
\"subprotocol\" : \"cov:observe\"\
}]\
}\
}\
}";

static const char thingDescription_cooling[] = "{\
\"actions\": {\
\"cooling\": {\
\"type\": \"string\",\
\"enum\": [\"on\", \"off\"],\
\"forms\": [{\
\"href\": \"coaps://[fc::2]/cooling\",\
\"cov:methodName\" : \"POST\"\
}]\
}\
}\
}";


RESOURCE(res_TD,
		"title=\"TD\";rt=\"thingDescription\"",
		td_get_handler,
		NULL,
		NULL,
		NULL);

static void td_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
	coap_set_header_content_format(response, APPLICATION_JSON);
	//snprintf((char *)buffer, COAP_MAX_CHUNK_SIZE, thingDescription);
	//snprintf((char *)buffer, COAP_MAX_CHUNK_SIZE, "no TD yet");
    if(NODEID == 1){
        snprintf((char *)buffer, sizeof(thingDescription_temp), thingDescription_temp);
    }
    else{
        snprintf((char *)buffer, sizeof(thingDescription_cooling), thingDescription_cooling);
    }
    

	coap_set_payload(response, (uint8_t *)buffer, strlen((char *)buffer));
}