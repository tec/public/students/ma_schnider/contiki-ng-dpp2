#include "contiki.h"
#include "coap-engine.h"
#include "dev/leds.h"

#include "sys/log.h"
#define LOG_MODULE "CoAP"
#define LOG_LEVEL LOG_LEVEL_CoAP

#define OVERHEATING_CHECK_INTERVAL		5000
#define OVERHEATING_CHECK_THRESHOLD		10

/* Simulate heating with led 1 */

static void heat_status_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void heat_post_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void overheating_obs_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void overheating_obs_periodic_handler(void);

static uint32_t overheating_check_counter = 0;
/*
RESOURCE(res_heating,
		"title=\"Heating\"",
		heat_status_get_handler,
		heat_post_handler,
		NULL,
		NULL);
for the demonstration, cooling is used instead heating
*/
RESOURCE(res_heating,
		"title=\"Cooling\"",
		heat_status_get_handler,
		heat_post_handler,
		NULL,
		NULL);


PERIODIC_RESOURCE(res_overheating,
				"title=\"Overheating\";obs",
				overheating_obs_get_handler,
				NULL,
				NULL,
				NULL,
				OVERHEATING_CHECK_INTERVAL,
				overheating_obs_periodic_handler);

static void heat_status_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
	//TODO get heating status
	char heating_status[5];

	unsigned int accept = -1;
  	coap_get_header_accept(request, &accept);

  	uint8_t led_status_mask = leds_get();
  	uint8_t led_num = 1;

  	if(led_status_mask & led_num){
  		snprintf(heating_status, 5, "on");
  	}
  	else{
  		snprintf(heating_status, 5, "off");
  	}

  	if(accept == -1 || accept == TEXT_PLAIN) {
	    coap_set_header_content_format(response, TEXT_PLAIN);
	    snprintf((char *)buffer, COAP_MAX_CHUNK_SIZE, "%s", heating_status);

	    coap_set_payload(response, (uint8_t *)buffer, strlen((char *)buffer));
	  } else if(accept == APPLICATION_JSON) {
	    coap_set_header_content_format(response, APPLICATION_JSON);
	    snprintf((char *)buffer, COAP_MAX_CHUNK_SIZE, "{'status':%s}", heating_status);

	    coap_set_payload(response, buffer, strlen((char *)buffer));
	  } else {
	    coap_set_status_code(response, NOT_ACCEPTABLE_4_06);
	    const char *msg = "Supporting content-types text/plain and application/json";
	    coap_set_payload(response, msg, strlen(msg));
	  }
}

static void heat_post_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
	const uint8_t *payload;
	int len = coap_get_payload(request, &payload);

	if(!strncmp((char*)payload, "on",len)){
		leds_single_on(LEDS_LED1);
		coap_set_status_code(response, CHANGED_2_04);
	}
	else if(!strncmp((char*)payload, "off",len)){
		leds_single_off(LEDS_LED1);
		coap_set_status_code(response, CHANGED_2_04);
		overheating_check_counter = 0;
	}
	else{
		//invalid argument
		coap_set_status_code(response, BAD_REQUEST_4_00);
	}
}

static void overheating_obs_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
	coap_set_header_content_format(response, TEXT_PLAIN);
	if(overheating_check_counter > OVERHEATING_CHECK_THRESHOLD){
		snprintf((char *)buffer, COAP_MAX_CHUNK_SIZE, "overheating!");
	}
	else{
		snprintf((char *)buffer, COAP_MAX_CHUNK_SIZE, "no overheating");
	}
	coap_set_payload(response, (uint8_t *)buffer, strlen((char *)buffer));
}

static void overheating_obs_periodic_handler(void){
	//simulate overheating: if led was on for more than 10 checks, we say its overheating
	int led_num = 1;
	if(leds_get() & led_num){
		overheating_check_counter++;
	}
	
	if(overheating_check_counter > OVERHEATING_CHECK_THRESHOLD){
		coap_notify_observers(&res_overheating);
	}
}
