#include "contiki.h"
#include "sensors/sht30.h"
#include "coap-engine.h"
#include "string.h"
#include "lib/json/jsonparse.h"
#include "stdlib.h"

#include "sys/log.h"
#define LOG_MODULE "CoAP"
#define LOG_LEVEL LOG_LEVEL_CoAP

static void temp_obs_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void temp_obs_periodic_handler(void);
static void temp_config_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);
static void temp_config_post_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

#define INTERVAL_MIN_DEFAULT			10
#define THRESHOLD_VALUE_DEFAULT			30
#define TRIGGERING_EDGE_DEFAULT			RISING
#define MAX_AGE 						60
#define TRIGGERING_EDGE_MAX_STR_SIZE	10


enum edge_type {RISING, FALLING};

struct TempConfig {
	enum edge_type triggering_edge;
	int threshold_value;
	int interval_min;
};

static struct TempConfig observe_config = {.triggering_edge=TRIGGERING_EDGE_DEFAULT, .threshold_value=THRESHOLD_VALUE_DEFAULT, .interval_min=INTERVAL_MIN_DEFAULT};
static int temperature_old = 0;
static int32_t interval_counter = INTERVAL_MIN_DEFAULT;

PERIODIC_RESOURCE(res_temp_obs, "title=\"Temperature\";obs", temp_obs_get_handler, NULL, NULL, NULL, 1000, temp_obs_periodic_handler);
RESOURCE(res_temp_conf, "title=\"Temp-config\"", temp_config_get_handler, temp_config_post_handler, NULL, NULL);

static void temp_obs_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
	/*
   * For minimal complexity, request query and options should be ignored for GET on observable resources.
   * Otherwise the requests must be stored with the observer list and passed by coap_notify_observers().
   * This would be a TODO in the corresponding files in contiki/apps/erbium/!
   */

  int temperature = sht30_sensor.value(SHT30_SENSOR_TEMP);
  temperature_old = temperature;

  unsigned int accept = -1;
  coap_get_header_accept(request, &accept);

  if(accept == -1 || accept == TEXT_PLAIN) {
    coap_set_header_content_format(response, TEXT_PLAIN);
    snprintf((char *)buffer, COAP_MAX_CHUNK_SIZE, "%d", temperature);

    coap_set_payload(response, (uint8_t *)buffer, strlen((char *)buffer));
  } else if(accept == APPLICATION_JSON) {
    coap_set_header_content_format(response, APPLICATION_JSON);
    snprintf((char *)buffer, COAP_MAX_CHUNK_SIZE, "{'temperature':%d}", temperature);

    coap_set_payload(response, buffer, strlen((char *)buffer));
  } else {
    coap_set_status_code(response, NOT_ACCEPTABLE_4_06);
    const char *msg = "Supporting content-types text/plain and application/json";
    coap_set_payload(response, msg, strlen(msg));
  }

  coap_set_header_max_age(response, MAX_AGE);

  /* The coap_subscription_handler() will be called for observable resources by the coap_framework. */
}

static void temp_obs_periodic_handler(void){
	++interval_counter;
	int temperature = sht30_sensor.value(SHT30_SENSOR_TEMP);

	if(interval_counter < observe_config.interval_min){
		temperature_old = temperature;
		return;
	}
	else if(observe_config.triggering_edge == FALLING){
		if((temperature_old >= observe_config.threshold_value) && (temperature < observe_config.threshold_value)){
			interval_counter = 0;
			coap_notify_observers(&res_temp_obs);
		}
	}else if(observe_config.triggering_edge == RISING){
		if((temperature_old <= observe_config.threshold_value) && (temperature > observe_config.threshold_value)){
			interval_counter = 0;
			coap_notify_observers(&res_temp_obs);
		}
	}
	temperature_old = temperature;
}

static void temp_config_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
	unsigned int accept = -1;
  	coap_get_header_accept(request, &accept);

  	//the config is a JSON object
  	if(accept == -1 || accept == TEXT_PLAIN || accept == APPLICATION_JSON) {
  		char triggering_edge_string[TRIGGERING_EDGE_MAX_STR_SIZE];
  		if(observe_config.triggering_edge == FALLING){
  			snprintf(triggering_edge_string, TRIGGERING_EDGE_MAX_STR_SIZE, "falling");
  		} else if(observe_config.triggering_edge == RISING){
  			snprintf(triggering_edge_string, TRIGGERING_EDGE_MAX_STR_SIZE, "rising");
  		}
    	coap_set_header_content_format(response, APPLICATION_JSON);
    	/*
    	snprintf((char *)buffer, COAP_MAX_CHUNK_SIZE, "{\"triggering_edge\":\"%s\",\"threshold_value\":%d,\"interval_min\":%d}",
    	 		triggering_edge_string, observe_config.threshold_value, observe_config.interval_min);
    	 		*/
    	//for testing: send big payload
    	snprintf((char*)buffer, COAP_MAX_CHUNK_SIZE, "der affe tanzt im affenhaus und ist ganz ausser sich. er isst eine banane und beruhigt sich wieder, doch es ist schon zu spaet, er hat das affenhaus verwuestet!");

    	coap_set_payload(response, (uint8_t *)buffer, strlen((char *)buffer));
  } else {
    	coap_set_status_code(response, NOT_ACCEPTABLE_4_06);
    	const char *msg = "Supporting content-types text/plain and application/json";
    	coap_set_payload(response, msg, strlen(msg));
  }
}

static void temp_config_post_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
	//TODO parse payload and read config
	const uint8_t *payload;
	int len = coap_get_payload(request, &payload);
	struct jsonparse_state state;
	int json_type;
	char string_key[32];
	char string_value[32];
	char *dummy;
	jsonparse_setup(&state, (char*)payload, len);

	//parse the JSON object. Only key value pairs are allowed, no nested objects or arrays
	while((json_type = jsonparse_next(&state)) != 0){
		if(json_type == JSON_TYPE_STRING){
			jsonparse_copy_value(&state, string_key, 32);
			if(!strcmp(string_key, "triggering_edge")){
				//next has to be a string, etiher rising or falling
				if((json_type = jsonparse_next(&state)) != 0){
					if(json_type != JSON_TYPE_STRING){
						coap_set_status_code(response, BAD_REQUEST_4_00);
						return;
					}
					jsonparse_copy_value(&state, string_value, 32);
					if(!strcmp(string_value, "rising")){
						observe_config.triggering_edge = RISING;
					}
					else if(!strcmp(string_value, "falling")){
						observe_config.triggering_edge = FALLING;
					}
					else{
						//unsupported argument
						coap_set_status_code(response, BAD_REQUEST_4_00);
						return;
					}
				}
				else{
					coap_set_status_code(response, BAD_REQUEST_4_00);
					return;
				}
			}
			else if(!strcmp(string_key, "threshold_value")){
				//next has to be string or int
				json_type = jsonparse_next(&state);
				if(json_type == JSON_TYPE_STRING){
					jsonparse_copy_value(&state, string_value, 32);
					observe_config.threshold_value = strtol(string_value, &dummy, 10);
				}
				else if(json_type == JSON_TYPE_INT){
					observe_config.threshold_value = jsonparse_get_value_as_int(&state);
				}
				else{
					coap_set_status_code(response, BAD_REQUEST_4_00);
					return;
				}
			}
			else if(!strcmp(string_key, "interval_min")){
				//next has to be string or int
				json_type = jsonparse_next(&state);
				if(json_type == JSON_TYPE_STRING){
					jsonparse_copy_value(&state, string_value, 32);
					observe_config.interval_min = strtol(string_value, &dummy, 10);
				}
				else if(json_type == JSON_TYPE_INT){
					observe_config.interval_min = jsonparse_get_value_as_int(&state);
				}
				else{
					coap_set_status_code(response, BAD_REQUEST_4_00);
					return;
				}
			}
			else{
				//ignore this, go to next
				continue;
			}
		}
		else{
			//all keys are strings, go to next
			continue;
		}
	}
	coap_set_status_code(response, CHANGED_2_04);
}