#include "contiki.h"
#include "sensors/sht30.h"
#include "coap-engine.h"

#include "sys/log.h"
#define LOG_MODULE "CoAP"
#define LOG_LEVEL LOG_LEVEL_CoAP

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset);

RESOURCE(res_temperature,
		"title=\"Temperature\"",
		res_get_handler,
		NULL,
		NULL,
		NULL);

static void res_get_handler(coap_message_t *request, coap_message_t *response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset){
	int temperature = sht30_sensor.value(SHT30_SENSOR_TEMP);

	unsigned int accept = -1;
  	coap_get_header_accept(request, &accept);

  	if(accept == -1 || accept == TEXT_PLAIN) {
	    coap_set_header_content_format(response, TEXT_PLAIN);
	    snprintf((char *)buffer, COAP_MAX_CHUNK_SIZE, "%d", temperature);

	    coap_set_payload(response, (uint8_t *)buffer, strlen((char *)buffer));
	  } else if(accept == APPLICATION_JSON) {
	    coap_set_header_content_format(response, APPLICATION_JSON);
	    snprintf((char *)buffer, COAP_MAX_CHUNK_SIZE, "{'temp':%d}", temperature);

	    coap_set_payload(response, buffer, strlen((char *)buffer));
	  } else {
	    coap_set_status_code(response, NOT_ACCEPTABLE_4_06);
	    const char *msg = "Supporting content-types text/plain and application/json";
	    coap_set_payload(response, msg, strlen(msg));
	  }
}