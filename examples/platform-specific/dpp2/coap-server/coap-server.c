/*
 * Copyright (c) 2006, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         A simple connectivity test. Broadcasting an incrementing number and printing received messages
 * \author
 *         Adam Dunkels <adam@sics.se>
 */

#include "contiki.h"
#include "dev/leds.h"
#include "net/netstack.h"
#include "net/ipv6/uip.h"
#include "net/ipv6/uip-debug.h"
#include "net/ipv6/simple-udp.h"
#include "net/ipv6/uip-icmp6.h"
#include "net/ipv6/uip-ds6.h"
#include "lib/sensors.h"

#include <stdio.h> /* For printf() */
#include "stdlib.h"  //for itoa
#include "string.h"

#include "driverlib.h"
#include "bolt/bolt.h"

#include "coap-engine.h"
#include "coap-blocking-api.h"
//#include "arch/dev/bme280/bme280-sensor.h"
#include "sensors/sht30.h"

#include "sys/log.h"
#define LOG_MODULE "UDP APP"
#define LOG_LEVEL LOG_LEVEL_INFO

#define UDP_PORT  61620


extern coap_resource_t res_temperature;
extern coap_resource_t res_temp_obs;
//extern coap_resource_t res_temp_conf;
extern coap_resource_t res_heating;
//extern coap_resource_t res_overheating;
extern coap_resource_t res_TD;

//TODO hardcoded server address for now, default ports
#define SERVER_EP "coap://[fe80:::::1]"

//#define DIRECTORY_SERVER_COAP_ADDR  "coap://[2001:67c:10ec:2a49:8000::23]"
//#define DIRECTORY_SERVER_COAP_ADDR    "coap://[2001:67c:10ec:2a49:8000::109d]"
//#define DIRECTORY_SERVER_COAP_ADDR    "coap://[2001:67c:10ec:2a49:8000::10cf]"
//#define DIRECTORY_SERVER_COAP_ADDR    "coap://[2001:67c:10ec:2a49:8000::1060]"
#define DIRECTORY_SERVER_COAP_ADDR    "coap://[fc::99]"
#define MAX_REGISTRATION_MESSAGE_SIZE 150

uint8_t registration_complete = 0;

/*---------------------------------------------------------------------------*/
PROCESS(hello_world_process, "Hello world process");
//PROCESS(er_coap_client, "Erbium CoAP Client");
PROCESS(er_coap_server, "Erbium CoAP Server");
PROCESS(er_coap_registration_client, "CoAP registration client");
//AUTOSTART_PROCESSES(&er_coap_registration_client, &er_coap_server, &hello_world_process);
//AUTOSTART_PROCESSES(&er_coap_server, &hello_world_process);
AUTOSTART_PROCESSES(&er_coap_server, &er_coap_registration_client);
/*---------------------------------------------------------------------------*/
//CoAP server process
PROCESS_THREAD(er_coap_server, ev, data){
  PROCESS_BEGIN();
  PROCESS_PAUSE();

  LOG_INFO("Starting CoAP Server\n\r");

  //activate sensors and bind resources to their Uri-Path
  SENSORS_ACTIVATE(sht30_sensor);
  coap_activate_resource(&res_temperature, "temp");
  coap_activate_resource(&res_temp_obs, "temp_obs");
  //coap_activate_resource(&res_temp_conf, "temp_conf");
  coap_activate_resource(&res_heating, "cooling");
  //coap_activate_resource(&res_overheating, "oh");
  coap_activate_resource(&res_TD, "TD");

  //application-specific events here
  while(1){
    PROCESS_WAIT_EVENT();
  }
  PROCESS_END();
}


void client_response_handler(coap_message_t *response){
  const uint8_t* payload;
  int len = coap_get_payload(response, &payload);

  if(response == NULL){
    LOG_INFO("Request timed out");
    return;
  }

  printf("received response len: %d\n\r", len);
  printf("|%.*s\n\r", len, (char *)payload);
}

void registration_response_handler(coap_message_t *response){
  if(response == NULL){
    LOG_INFO("Registration timed out");
    return;
  }
  else{
    coap_message_type_t response_type = response->type;
    coap_status_t response_code = response->code;
    if(response_type == COAP_TYPE_ACK){
      printf("received ACK for registration message, code: %u\n\r",response_code);
      registration_complete = 1;
    }
  }
}

//CoAP registration process at directory server

PROCESS_THREAD(er_coap_registration_client, ev, data){
  static coap_endpoint_t server_ep;
  static struct etimer coap_registration_timer;
  PROCESS_BEGIN();

  registration_complete = 0;
  static coap_message_t request[1];
  uip_ds6_addr_t *lladdr;
  lladdr = uip_ds6_get_link_local(-1);
  coap_endpoint_parse(DIRECTORY_SERVER_COAP_ADDR, strlen(DIRECTORY_SERVER_COAP_ADDR), &server_ep);
  char registration_message_complete[MAX_REGISTRATION_MESSAGE_SIZE];
  int payload_size;
  {
    char registration_message_string_start[] = "{\"";
    char registration_message_string_mid[] = "\":\"";
    char registration_message_string_end[] = "\"}"; 
    char linkaddr_hexstring_total[20];
    snprintf(linkaddr_hexstring_total, sizeof(linkaddr_hexstring_total), "%02X%02X.%02X%02X.%02X%02X.%02X%02X", linkaddr_node_addr.u8[0],linkaddr_node_addr.u8[1],linkaddr_node_addr.u8[2],linkaddr_node_addr.u8[3],linkaddr_node_addr.u8[4],linkaddr_node_addr.u8[5],linkaddr_node_addr.u8[6],linkaddr_node_addr.u8[7]);
    char ip_hexstring_total[40];
    //global address prefix has to be extracted, for now hardcoded
    snprintf(ip_hexstring_total, sizeof(ip_hexstring_total), "FC00:0000:0000:0000:%02X%02X:%02X%02X:%02X%02X:%02X%02X",lladdr->ipaddr.u8[8], lladdr->ipaddr.u8[9], lladdr->ipaddr.u8[10],lladdr->ipaddr.u8[11],lladdr->ipaddr.u8[12],lladdr->ipaddr.u8[13],lladdr->ipaddr.u8[14],lladdr->ipaddr.u8[15]);
    //snprintf(ip_hexstring_total, sizeof(ip_hexstring_total), "%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X:%02X%02X",lladdr->ipaddr.u8[0], lladdr->ipaddr.u8[1], lladdr->ipaddr.u8[2], lladdr->ipaddr.u8[3],lladdr->ipaddr.u8[4],lladdr->ipaddr.u8[5],lladdr->ipaddr.u8[6],lladdr->ipaddr.u8[7],lladdr->ipaddr.u8[8], lladdr->ipaddr.u8[9], lladdr->ipaddr.u8[10],lladdr->ipaddr.u8[11],lladdr->ipaddr.u8[12],lladdr->ipaddr.u8[13],lladdr->ipaddr.u8[14],lladdr->ipaddr.u8[15]);
    payload_size = snprintf(registration_message_complete, sizeof(registration_message_complete), "%s%s%s%s%s", registration_message_string_start, linkaddr_hexstring_total, registration_message_string_mid, ip_hexstring_total, registration_message_string_end);
  }
  if(payload_size < 0){
    LOG_WARN("could not create registration message");
  }
  printf("registration message: %s\n\r",registration_message_complete);
  //for now reregistration every 5 min
  etimer_set(&coap_registration_timer, CLOCK_SECOND*60*5);
  coap_init_message(request, COAP_TYPE_CON, COAP_POST, 0);
  coap_set_header_uri_path(request, "devices");
  coap_set_payload(request, registration_message_complete, payload_size);
  coap_set_header_content_format(request, APPLICATION_JSON);

  while(!registration_complete){
    COAP_BLOCKING_REQUEST(&server_ep, request, registration_response_handler);
    LOG_INFO("registration message sent\n\r");

    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&coap_registration_timer));
    etimer_reset(&coap_registration_timer);
  }
  LOG_INFO("Registration completed, exit registration process\n\r");
  PROCESS_END();
}

//CoAP client process
/*
PROCESS_THREAD(er_coap_client, ev, data){
  static coap_endpoint_t server_ep;
  static struct etimer coap_request_timer;
  PROCESS_BEGIN();

  etimer_set(&coap_request_timer, CLOCK_SECOND * 30);
  static coap_message_t request[1];
  coap_endpoint_parse(SERVER_EP,strlen(SERVER_EP), &server_ep);

  while(1){

    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&coap_request_timer));

    
    if(NODEID != 1){

      coap_init_message(request, COAP_TYPE_CON, COAP_GET, 0);
      coap_set_header_uri_path(request, "temp");
      

      COAP_BLOCKING_REQUEST(&server_ep, request, client_response_handler);

      LOG_INFO("Coap request done\n\r");
    }

    etimer_reset(&coap_request_timer);
  }
  PROCESS_END();
}*/


//this process demonstrates that the node is still alive
PROCESS_THREAD(hello_world_process, ev, data)
{
  static struct etimer timer;
  static struct etimer initial_wait_timer;
  static int temperature;

  PROCESS_BEGIN();

  etimer_set(&initial_wait_timer, CLOCK_SECOND / 100);

  //wait a moment for peripherals to become active
  PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&initial_wait_timer));
  SENSORS_ACTIVATE(sht30_sensor);
  /* Setup a periodic timer that expires after 10 seconds. */
  etimer_set(&timer, CLOCK_SECOND * 30);


  while(1) {
    //printf("Hello, world\n");
    leds_single_toggle(LEDS_LED2);
    temperature = sht30_sensor.value(SHT30_SENSOR_TEMP);

    LOG_INFO("Node measured temp: %d\n\r", temperature);

    /* Wait for the periodic timer to expire and then restart the timer. */
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&timer));
    etimer_reset(&timer);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
