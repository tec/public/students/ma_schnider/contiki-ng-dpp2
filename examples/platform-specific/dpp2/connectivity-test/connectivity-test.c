/*
 * Copyright (c) 2006, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         A simple connectivity test. Broadcasting an incrementing number and printing received messages
 * \author
 *         Adam Dunkels <adam@sics.se>
 */

#include "contiki.h"
#include "dev/leds.h"
#include "net/netstack.h"
#include "net/ipv6/uip.h"
#include "net/ipv6/uip-debug.h"
#include "net/ipv6/simple-udp.h"
#include "net/ipv6/uip-icmp6.h"

#include <stdio.h> /* For printf() */
#include "stdlib.h"  //for itoa
#include "string.h"

#include "driverlib.h"
#include "bolt/bolt.h"

#include "sys/log.h"
#define LOG_MODULE "UDP APP"
#define LOG_LEVEL LOG_LEVEL_INFO

#define UDP_PORT  61620

static struct simple_udp_connection udp_conn;
static struct uip_icmp6_echo_reply_notification echo_reply_notification;
static volatile int waiting_for_ping_reply;

/*---------------------------------------------------------------------------*/
PROCESS(hello_world_process, "Hello world process");
PROCESS(connectiviy_process, "Connectivity test process");
PROCESS(ping_process, "Ping process");
//AUTOSTART_PROCESSES(&hello_world_process, &connectiviy_process);
AUTOSTART_PROCESSES(&ping_process, &hello_world_process);
/*---------------------------------------------------------------------------*/
//this process demonstrates that the node is still alive
PROCESS_THREAD(hello_world_process, ev, data)
{
  static struct etimer timer;

  PROCESS_BEGIN();

  /* Setup a periodic timer that expires after 10 seconds. */
  etimer_set(&timer, CLOCK_SECOND * 10);


  while(1) {
    //printf("Hello, world\n");
    leds_single_toggle(LEDS_LED2);

    /* Wait for the periodic timer to expire and then restart the timer. */
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&timer));
    etimer_reset(&timer);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
static void receiver_callback(struct simple_udp_connection *c,
                              const uip_ipaddr_t *sender_addr,
                              uint16_t sender_port,
                              const uip_ipaddr_t *receiver_addr,
                              uint16_t receiver_port,
                              const uint8_t *data,
                              uint16_t datalen)
{
  LOG_INFO("Data received from ");
  LOG_INFO_6ADDR(sender_addr);
  LOG_INFO(" on port %d from port %d with length %d:  %s \n\r",
         receiver_port, sender_port, datalen, data);
}

static void
echo_reply_handler(uip_ipaddr_t *source, uint8_t ttl, uint8_t *data, uint16_t datalen){
  LOG_INFO("received ping reply from ");
  LOG_INFO_6ADDR(source);
  LOG_INFO("\r");
  waiting_for_ping_reply = 0;
  process_poll(&ping_process);
}

PROCESS_THREAD(ping_process, ev, data){
  static struct etimer ping_timeout_timer;
  static uip_ipaddr_t ping_dest_addr;

  PROCESS_BEGIN();

  //send to link-local node 1
  ping_dest_addr.u8[0] = 0xfe;
  ping_dest_addr.u8[1] = 0x80;
  ping_dest_addr.u8[2] = 0x00;
  ping_dest_addr.u8[3] = 0x00;
  ping_dest_addr.u8[4] = 0x00;
  ping_dest_addr.u8[5] = 0x00;
  ping_dest_addr.u8[6] = 0x00;
  ping_dest_addr.u8[7] = 0x00;
  ping_dest_addr.u8[8] = 0x00;
  ping_dest_addr.u8[9] = 0x00;
  ping_dest_addr.u8[10] = 0x00;
  ping_dest_addr.u8[11] = 0x00;
  ping_dest_addr.u8[12] = 0x00;
  ping_dest_addr.u8[13] = 0x00;
  ping_dest_addr.u8[14] = 0x00;
  ping_dest_addr.u8[15] = 0x01;
  etimer_set(&ping_timeout_timer, CLOCK_SECOND * 15);
  uip_icmp6_echo_reply_callback_add(&echo_reply_notification, echo_reply_handler);
  waiting_for_ping_reply = 0;

  while(1){
    if(NODEID != 1){
      //TODO add callback, handle response
      uip_icmp6_send(&ping_dest_addr, ICMP6_ECHO_REQUEST, 0, 4);    //code 0, payload len 4, copied from shell code
      waiting_for_ping_reply = 1;
      //PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&ping_timeout_timer) || (waiting_for_ping_reply == 0));
      //PROCESS_YIELD_UNTIL(etimer_expired(&ping_timeout_timer) || (ev == PROCESS_EVENT_POLL));
      PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&ping_timeout_timer) || (ev == PROCESS_EVENT_POLL));
      if(waiting_for_ping_reply == 0){
        LOG_INFO("ping delay: %lu ms\r", (1000*(clock_time() - ping_timeout_timer.timer.start))/CLOCK_SECOND);
      }
      else{
        LOG_INFO("ping timeout\r");
      }
      etimer_reset(&ping_timeout_timer);
    }
    else{
      PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&ping_timeout_timer));
      etimer_reset(&ping_timeout_timer);
    }
  }

  PROCESS_END();
}

//send dummy messages, print received messages
PROCESS_THREAD(connectiviy_process, ev, data)
{
  static uint8_t payload = 1;
  char tx_buffer[20];
  //uint8_t rx_buffer[128];
  
  //uip_ipaddr_t broadcast_addr;
  static uip_ipaddr_t dest_addr;
  static struct etimer tx_timer;


  PROCESS_BEGIN();

  //send to link-local node 1
  dest_addr.u8[0] = 0xfe;
  dest_addr.u8[1] = 0x80;
  dest_addr.u8[2] = 0x00;
  dest_addr.u8[3] = 0x00;
  dest_addr.u8[4] = 0x00;
  dest_addr.u8[5] = 0x00;
  dest_addr.u8[6] = 0x00;
  dest_addr.u8[7] = 0x00;
  //magic: have to add this to get correct lladdr
  dest_addr.u8[8] = 0x00;
  dest_addr.u8[9] = 0x00;
  dest_addr.u8[10] = 0x00;
  dest_addr.u8[11] = 0x00;
  dest_addr.u8[12] = 0x00;
  dest_addr.u8[13] = 0x00;
  dest_addr.u8[14] = 0x00;
  dest_addr.u8[15] = 0x01;
  //uip_create_linklocal_allnodes_mcast(&broadcast_addr);
  simple_udp_register(&udp_conn, UDP_PORT, NULL, UDP_PORT, receiver_callback);
  etimer_set(&tx_timer, CLOCK_SECOND * 30);

  while(1){
    leds_single_toggle(LEDS_LED1);
    sprintf(tx_buffer, "%d", payload);

    //LOG_INFO("sending broadcast with payload: %s , payload length %u\n", tx_buffer, strlen(tx_buffer));
    if(NODEID != 1){
        LOG_INFO("sending broadcast");
        simple_udp_sendto(&udp_conn, tx_buffer, strlen(tx_buffer), &dest_addr);
    }

    //just send a simple 1 byte counter payload for bolt debugging
    //NETSTACK_RADIO.send(tx_buffer, 1);

    payload++;

    //debugging bolt
    uint8_t bolt_ind_value = MAP_GPIO_getInputPinValue(GPIO_PORT_P4, GPIO_PIN4);
    uint8_t bolt_ind_out_value = MAP_GPIO_getInputPinValue(GPIO_PORT_P4, GPIO_PIN3);
    //uint32_t bolt_read_count = bolt_read(rx_buffer);
    LOG_INFO("ind: %u, ind out: %u\n", bolt_ind_value, bolt_ind_out_value);



    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&tx_timer));
    etimer_reset(&tx_timer);
  }


  PROCESS_END();
}